/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Copyright (C) 2011-2021 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    rThetaTopoSets

Description
    This utility creates a series of radial-azimuthal topoSets in a 2D r-Theta
    cylindrical geometry. The naming convention of the sets in output is 
    "r_i_w_j", where i represents the number of radial slices (increasing from
    the center towards the outer radius), and j is the wedge number (starting 
    from the x axis and increasing counter-clockwise).

Usage:
    In file system/rThetaTopoSetsDict:
    
    \verbatim
    //- Type of the topoSet to be created
    type    cellSet;

    //- Name of the fuel cellzone
    fuelCellZoneName        "fuel";

    //- Outer radius of the fuel pin
    fuelOuterRadius         0.004435;

    //- Angle covered by the geometry in degrees
    angleGeometryDegrees    360;

    //- Radial and azimutal divisions
    radialSplit             fromMesh; //uniform;
    azimuthalSplit          uniform; // fromMesh;

    //- Optional r-theta divisions (only read if split == "uniform")
    //nRadial                 10;
    nAzimuthal              10;
    \endverbatim    

\mainauthor
    E. Brunetto - EPFL (ECOLE POLYTECHNIQUE FEDERALE DE LAUSANNE, 
    Switzerland, Laboratory for Reactor Physics and Systems Behaviour)\n

\*---------------------------------------------------------------------------*/

#include "argList.H"
#include "Time.H"
#include "polyMesh.H"
#include "globalMeshData.H"
#include "timeSelector.H"
#include "IOobjectList.H"
#include "cellZoneSet.H"
#include "faceZoneSet.H"
#include "pointZoneSet.H"
#include "systemDict.H"
#include "SortableList.H"
#include "simpleMatrix.H"

using namespace Foam;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

void printMesh(const Time& runTime, const polyMesh& mesh)
{
    Info<< "Time:" << runTime.timeName()
        << "  cells:" << mesh.globalData().nTotalCells()
        << "  faces:" << mesh.globalData().nTotalFaces()
        << "  points:" << mesh.globalData().nTotalPoints()
        << "  patches:" << mesh.boundaryMesh().size()
        << "  bb:" << mesh.bounds() << nl;
}


polyMesh::readUpdateState meshReadUpdate(polyMesh& mesh)
{
    polyMesh::readUpdateState stat = mesh.readUpdate();

    switch(stat)
    {
        case polyMesh::UNCHANGED:
        {
            Info<< "    mesh not changed." << endl;
            break;
        }
        case polyMesh::POINTS_MOVED:
        {
            Info<< "    points moved; topology unchanged." << endl;
            break;
        }
        case polyMesh::TOPO_CHANGE:
        {
            Info<< "    topology changed; patches unchanged." << nl
                << "    ";
            printMesh(mesh.time(), mesh);
            break;
        }
        case polyMesh::TOPO_PATCH_CHANGE:
        {
            Info<< "    topology changed and patches changed." << nl
                << "    ";
            printMesh(mesh.time(), mesh);

            break;
        }
        default:
        {
            FatalErrorInFunction
                << "Illegal mesh update state "
                << stat  << abort(FatalError);
            break;
        }
    }
    return stat;
}

// Function to compute the distance from origin of a input cell center
Foam::scalar calcRadius
(
    const vector& cellCenter
)
{
    const scalar x(cellCenter[0]);
    const scalar y(cellCenter[1]);

    return Foam::sqrt(pow(x,2)+pow(y,2));
}

// Function to compute the angular distance of a cell center wrt x-axis(theta=0)
Foam::scalar calcAngle
(
    const vector& cellCenter
)
{
    const scalar x(cellCenter[0]);
    const scalar y(cellCenter[1]);

    // Build a vector from origin to cell center
    vector pos(x,y,0);

    // Define vector oriented as x-axis
    const vector ex(1,0,0);

    // Find angle in radians between two vectors
    scalar angle = ( Foam::acos((pos & ex)/(mag(pos)*mag(ex))) );

    // Consider angles bigger than 180 deg
    if( y < 0 )
    {
        angle = 2*constant::mathematical::pi - angle;
    }

    // Conversion in degrees
    angle *= 180.0/constant::mathematical::pi;

    return angle;
}

// Compute radial and azimuthal mesh cells
Foam::List<int> calcRThetaMesh
(
    polyMesh& mesh,
    word fuelName
)
{
    // Initialize result list
    List<int> result(0);

    forAll( mesh.cellZones(), zoneI )
    {
        if (mesh.cellZones()[zoneI].name() == (fuelName))
        {
            const labelList& fuelCells(mesh.cellZones()[zoneI]);
        
            // Compute azimuthal mesh cells

            // Store a reference radius value
            const scalar radius = calcRadius(mesh.cellCentres()[fuelCells[0]]);

            // Initialize counter
            int counterAz(0);

            // Find number of radii equal to the reference one
            forAll(fuelCells, idx)
            {
                const label cellI = fuelCells[idx];
                const scalar r = calcRadius(mesh.cellCentres()[cellI]);

                if( mag(radius - r) < 1e-10 )
                {
                    counterAz++;
                }
            }

            // Compute radial mesh cells
            int counterRad = fuelCells.size() / counterAz;

            Info << nl  << "Calculating mesh info ... " << nl
                 << tab << "Mesh azimuthal divisions : " << counterAz << nl
                 << tab << "Mesh radial divisions : " << counterRad << endl;

            // Return result in a list of two values (r,theta)
            result.append(counterRad);
            result.append(counterAz);
        }
    }

    return result;
}

// Compute non uniform radial thickness in case of radial mesh grading
Foam::SortableList<scalar> calcThickness
(
    polyMesh& mesh,
    word fuelName
)
{
    // Initialize list of cell thicknesses along a radius
    SortableList<scalar> thickness(0);
    SortableList<scalar> radii(0);

    // Store points and faces of the mesh
    const pointField& pp = mesh.points();
    const faceList&   ff = mesh.faces();

    forAll( mesh.cellZones(), zoneI )
    {
        if (mesh.cellZones()[zoneI].name() == (fuelName))
        {
            const labelList& fuelCells(mesh.cellZones()[zoneI]);
        
            // Store a reference angle value wrt to (1,1,0) and its direction 
            const scalar angle = calcAngle(mesh.cellCentres()[fuelCells[0]]);
            const scalar angleR = angle*constant::mathematical::pi/180.0;
            const vector dir(Foam::cos(angleR), Foam::sin(angleR), 0);

            // Store cell dimensions along dir
            forAll(fuelCells, idx)
            {
                const label cellI = fuelCells[idx];
                const scalar a = calcAngle(mesh.cellCentres()[cellI]);

                if( mag(angle - a) < 1e-6 )
                {

                    // Get labels of cellI vertices
                    const labelList pLabels(mesh.cells()[cellI].labels(ff));
                    // Initialize pointField of cellI
                    pointField pLocal(pLabels.size(), vector::zero);

                    // Store local cellI points
                    forAll (pLabels, pointi)
                    {
                        pLocal[pointi] = pp[pLabels[pointi]];
                    }

                    // Compute dimension of the cell along direction dir
                    thickness.append
                    (
                        Foam::max(pLocal & dir) - Foam::min(pLocal & dir)
                    );

                    // Store radius of the cell as well
                    radii.append(calcRadius(mesh.cellCentres()[cellI]));
                }
            }
        }
    }

    // Sort radii increasingly and thicknesses decreasingly
    radii.sort();
    thickness.reverseSort();

    return thickness;
}

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
    timeSelector::addOptions(true, false);
    #include "addDictOption.H"
    #include "addRegionOption.H"
    argList::addBoolOption
    (
        "noSync",
        "do not synchronise selection across coupled patches"
    );

    #include "setRootCase.H"
    #include "createTime.H"

    instantList timeDirs = timeSelector::selectIfPresent(runTime, args);

    #include "createNamedPolyMesh.H"

    const dictionary dict(systemDict("rThetaTopoSetsDict", args, mesh));

    forAll(timeDirs, timeI)
    {
        runTime.setTime(timeDirs[timeI], timeI);
        Info<< "Time = " << runTime.timeName() << endl;

        // Optionally re-read mesh
        meshReadUpdate(mesh);

        // Read dictionary settings
        const word   setType(dict.lookup("type"));
        const word   fuelName(dict.lookup("fuelCellZoneName"));
        const scalar rOut(dict.lookup<scalar>("fuelOuterRadius"));
        const scalar angleGeom(dict.lookup<scalar>("angleGeometryDegrees"));

        const word   radSplit(dict.lookup("radialSplit"));
        const word   azSplit(dict.lookup("azimuthalSplit"));

        // Compute radial and azimuthal fuel mesh division
        List<int> rThetaMesh = calcRThetaMesh(mesh, fuelName);

        const int nRadial
        (
            (radSplit == "uniform") ?
            dict.lookup<scalar>("nRadial")
            :
            rThetaMesh[0]
        );
        
        const int nAzimuthal
        (
            (azSplit == "uniform") ?
            dict.lookup<scalar>("nAzimuthal")
            :
            rThetaMesh[1]
        );

        // Check that the divisions are coherent to the mesh grid
        if
        ( 
            rThetaMesh[0] % nRadial != 0 
            ||
            rThetaMesh[1] % nAzimuthal != 0 
        )
        {
            FatalErrorInFunction
              << tab << "The number of radial and azimuthal divisions " << endl
              << tab << "of the mesh must be a multiple of the number " << endl
              << tab << "of radial and of azimuthal divisions of the sets !" << endl
              << exit(FatalError);
        }
        
        // Create list of autoPtr of toposets
        List<autoPtr<topoSet>> topoSets;
        topoSets.clear();

        // Initialize empty topoSets with naming convention r-w
        for (int i = 0; i < nRadial; i++) 
        {
            for (int j = 0; j < nAzimuthal; j++) 
            {
                std::ostringstream s;
                s << "r" << i << "w" << j;
                const word setNameI (s.str());
                topoSets.append(topoSet::New(setType, mesh, setNameI, 10000));
            }
        }


        
        // Create List of outer radii bounding the different radial zones
        const List<scalar> uniformThickness(nRadial, rOut/nRadial);

        List<scalar> outerRadii(1, 0.0);
        const List<scalar> thickness
        (
            (radSplit == "fromMesh") ?
            calcThickness(mesh,fuelName)
            :
            uniformThickness
        );

        scalar rEnd(0.0);
        forAll(uniformThickness, i)
        {
            rEnd += thickness[i];
            outerRadii.append(rEnd);
        }

        // Create List of angular wedges bounding the different azimuthal zones
        const scalar wedgesAngle(angleGeom/nAzimuthal);
        List<scalar> angularWedges(1, 0.0);

        scalar angleEnd(0.0);
        for (int i = 0; i < nAzimuthal; i++) 
        {
            angleEnd += wedgesAngle;
            angularWedges.append(angleEnd);
        }

        // Loop through all the fuel cells and assign it to the corresponding set
        forAll( mesh.cellZones(), zoneI )
        {
            // Check if cellZone name is equal to fuelName
            if (mesh.cellZones()[zoneI].name() == (fuelName))
            {
                const labelList& fuelCells = mesh.cellZones()[zoneI];

                // Assign each cell of the cellZone to a proper topoSet
                forAll(fuelCells, idx)
                {
                    const label cellI = fuelCells[idx];

                    scalar r = calcRadius(mesh.cellCentres()[cellI]);
                    scalar a = calcAngle(mesh.cellCentres()[cellI]);

                    scalar i(-1);
                    scalar j(-1);
                    bool rFound(false);
                    bool wFound(false);
                    while(!rFound)
                    {
                        i++;
                        
                        if( (r >= outerRadii[i]) and (r <= outerRadii[i+1]) )
                        {
                            rFound = true;
                            
                            j = -1;
                            while(!wFound)
                            {
                                j++;
                                if( (a >= angularWedges[j]) and (a <= angularWedges[j+1]) )
                                {
                                    wFound = true;
                                }
                            }
                        }
                    }

                    // If cell belogs radially and azimuthally assign it to
                    // the r-i-w-j set
                    if( rFound and wFound )
                    {
                        forAll(topoSets, t)
                        {
                            std::ostringstream s;
                            s << "r" << i << "w" << j;
                            const word setNameI (s.str());
                            
                            if( topoSets[t]().name() == setNameI)
                            {
                                topoSets[t]().insert(cellI);
                            }
                        }

                    }
                } 

            }

            forAll(topoSets,i)
            {
                topoSets[i]().write();
                fileHandler().flush();
            }
        }
    }

    Info<< "End\n" << endl;

    return 0;
}


// ************************************************************************* //
