# historySimplifier

historySimplifier.py is a Python3 script for the pre-processing of the power 
history data needed to perform a fuel behavior simulation in OFFBEAT. The
[Ramer-Douglas-Peucker](https://towardsdatascience.com/simplify-polylines-with-the-douglas-peucker-algorithm-ac8ed487a4a1) 
algorithm is used to reduce the number of points in the power history with 
the objective to speed up the OFFBEAT calculations whenever deemed appropriate.

## Required python packages 

The required libraries to run the script can be installed with the package 
manager [pip](https://pip.pypa.io/en/stable/).

```bash
# For data plotting :
pip3 install matplotlib

# For file handling and array operations:
pip3 install numpy

# For the Ramer-Douglas-Peucker algorithm:
pip3 install rdp
```

## Available settings

The tool is structured in two files:

* *historySimplifierDict*: A python dictionary to set up the desired 
configurations for the script to run

* *historySimplifier.py*: The actual python script that performs the history
simplifications.

The user shall only modify the *historySimplifierDict* file according to his 
preferences. The user should specify the following entries:

* *lhgrPath*: The path of the power history file to be reduced containing the
*lhgr* list.

* *timePointsPath*: The path of the power history file to be reduced containing the
*timePoints* list.

* *axialProfilePath*: Optional entry for the path of the axial profile file 
containing the *data* time list.

* *epsilon*: Refinement parameter for the points reduction (the higher the 
parameter, the coarser the power history).

* *refinedInterval*: Boolean switch to avoid point reduction for specific 
time interval/s. It will maintain the power history unchanged for the specified
intervals.

* *tStart*: Array where are specified the start time/s for the intervals where 
the power history shall remain unchanged.

* *tEnd*: Array where are specified the end time/s for the intervals where 
the power history shall remain unchanged.

* *plotResult*: Boolean switch to visualize the data before and after the pre-processing.

Notice : *lhgrPath*, *timePointsPath* and *axialProfilePath* can point to the 
same file containing the three lists (*lhgr*, *timePoints* and *data*).  
## Usage

```python
python3 historySimplifier.py
```

Notice: the dictionary input file *historySimplifierDict* must be located in
the same folder where the script is being run.
