///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include "FuelDensity.h"

/// FuelDensity

void FuelDensity( )
{
  Fuel_density[1] = Fuel_density[0]; // (kg/m3)
}
