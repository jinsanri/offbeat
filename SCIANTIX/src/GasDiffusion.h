///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include "GlobalVariables.h"
#include "Solver.h"
#include "FissionYield.h"
#include "GasDiffusionCoefficient.h"
#include "NucleationRate.h"
#include "ResolutionRate.h"
#include "TrappingRate.h"
#include <iostream>

void GasDiffusion( );
