///////////////////////////////////////////
//                                       //
//           S C I A N T I X             //
//           ---------------             //
//                                       //
//  Version: 1.4                         //
//  Year   : 2019                        //
//  Authors: D. Pizzocri and T. Barani   //
//                                       //
///////////////////////////////////////////

#include "GlobalVariables.h"
#include "SetVariables.h"
#include "FuelDensity.h"
#include "OxygenToMetalRatio.h"
#include "Burnup.h"
#include "FuelMicroStructure.h"
#include "InertGasBehavior.h"
#include "UpdateVariables.h"
#include "OutputWriting.h"

void Sciantix(int Sciantix_options[], double Sciantix_history[], double Sciantix_variables[], double Sciantix_scaling_factors[]);
