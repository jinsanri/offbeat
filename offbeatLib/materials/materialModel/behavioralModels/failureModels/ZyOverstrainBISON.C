/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "ZyOverstrainBISON.H"
#include "addToRunTimeSelectionTable.H"
#include "offbeatTime.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(ZyOverstrainBISON, 0);
    addToRunTimeSelectionTable(failureModel, ZyOverstrainBISON, dictionary);
}

// * * * * * * * * * * * * * Static Member Functions * * * * * * * * * * * * //


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::ZyOverstrainBISON::ZyOverstrainBISON
(
    const fvMesh& mesh,
    const dictionary& dict
)
:
    failureModel(mesh, dict),
    hoopStrainLimit_(dict.lookupOrDefault<scalar>("hoopStrainLimit", 0.336))
{    
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::ZyOverstrainBISON::~ZyOverstrainBISON()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

bool Foam::ZyOverstrainBISON::isFailed
(
    const labelList& addr
)
{
    if( !mesh_.foundObject<volSymmTensorField>("epsilonCreep") )
    {
        FatalErrorInFunction()
        << "The selected failure criterion needs the creep model to be activated.\n"
        << exit(FatalError); 
    }

    // Assign mapper object to pointer
    if(mapper_ == nullptr)
    {
        mapper_ = &mesh_.lookupObject<sliceMapper>("sliceMapper");
    }

    // Constant reference to the epsilonCreep field
    const volSymmTensorField& epsilonCreep(mesh_.lookupObject<volSymmTensorField>("epsilonCreep"));

    // Set failure switch to false
    failed_ = false;

    // Initialize epsilonCreepCyl as a copy of epsilon Creep and * to zero
    volSymmTensorField epsilonCreepCyl("epsilonCreepCyl", epsilonCreep);
    epsilonCreepCyl *= scalar(0);

    // Transform the epsilonCreep tensor in cylindrical coordinates
    forAll(addr, addrI)
    {
        const label cellI = addr[addrI];

        tensor R = tensor::zero;

        const vector& Ci = mesh_.C()[cellI];
        const scalar  radius = sqrt(Ci[0]*Ci[0] + Ci[1]*Ci[1]);

        R.xx() =  Ci[0]/radius;
        R.xy() =  Ci[1]/radius;
        R.yy() =  Ci[0]/radius;
        R.yx() = -Ci[1]/radius;
        R.zz() = 1;

        epsilonCreepCyl[cellI] = symm( R & epsilonCreep[cellI] & R.T() );
    }

    // Calculate the slice-average values of epsilonCreep 
    const symmTensorField& epsilonCreepSliceAvg(mapper_->sliceAverage(epsilonCreepCyl));

    const offbeatTime& userRunTime(
        refCast<const offbeatTime>(mesh_.time()));

    // Check if the avg epsilonCreep per slice overcmes the hoop strain
    forAll(epsilonCreepSliceAvg, i)
    {
        const symmTensor e = epsilonCreepSliceAvg[i];

        if ( e.component(symmTensor::YY) >= hoopStrainLimit_ )
        {
            if( !mesh_.foundObject<volScalarField>("failedMaterial"))
            {
                // Initialize failedMaterial field (from parent class "failureModel") 
                initializeFailedMaterialField();
            }

            failed_ = true;

            // Print out information about failure
            Info << "Failed at time " <<  userRunTime.userTime() << userRunTime.unit() 
                 << ", in slice n. " << i 
                 << ", with slice-avg. creep hoop strain = " 
                 << e.component(symmTensor::YY)
                 << endl;

            // Set the field failedMaterial_ to 1 in correspondance of the slice
            const labelList addrFailed = mapper_->sliceAddrList()[i];
            forAll( addrFailed, addrI )
            {
                const label cellI = addrFailed[addrI];
                failedMaterial_()[cellI] = 1;
            }
        }
    }

    return failed_;

}


// ************************************************************************* //
