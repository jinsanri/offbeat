/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "ZyPlasticInstabilityBISON.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
    defineTypeNameAndDebug(ZyPlasticInstabilityBISON, 0);
    addToRunTimeSelectionTable(failureModel, ZyPlasticInstabilityBISON, dictionary);
}

// * * * * * * * * * * * * * Static Member Functions * * * * * * * * * * * * //


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * Protected Member Functions  * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::ZyPlasticInstabilityBISON::ZyPlasticInstabilityBISON
(
    const fvMesh& mesh,
    const dictionary& dict
)
:
    failureModel(mesh, dict),
    burstStrainRate_(dict.lookupOrDefault<scalar>("burstStrainRate", 2.78e-2))
{    
}

// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::ZyPlasticInstabilityBISON::~ZyPlasticInstabilityBISON()
{}

// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

bool Foam::ZyPlasticInstabilityBISON::isFailed
(
    const labelList& addr
)
{
    if
    ( 
        !mesh_.foundObject<volScalarField>("DepsilonCreepEq")
        || 
        !mesh_.foundObject<volScalarField>("DEpsilonPEq")
    )
    {
        FatalErrorIn("Foam::ZyPlasticInstabilityBISON::checkFailure(const labelList& addr)")
        << "The selected failure criterion needs the plasticCreep model to be activated.\n"
        << exit(FatalError); 
    }

    //- Set failure switch to false
    failed_ = false;

    //- Set counter of failed cells to 0
    scalar counter(0);

    //- Constant reference to the fields
    const volScalarField& DepsilonCreepEq(mesh_.lookupObject<volScalarField>("DepsilonCreepEq"));
    const volScalarField& DepsilonPEq    (mesh_.lookupObject<volScalarField>("DEpsilonPEq"));

    forAll(addr, addrI)
    {
        const label cellI = addr[addrI];

        //- Compute effective plastic strain rate
        const scalar plasticStrainRateEff
        (
            (DepsilonCreepEq.internalField()[cellI] + DepsilonPEq.internalField()[cellI])
            /
            mesh_.time().deltaTValue()
        );

        if ( plasticStrainRateEff >= burstStrainRate_)
        {
            if( !mesh_.foundObject<volScalarField>("failedMaterial"))
            {
                //- Initialize failedMaterial field (from parent class "failureModel") 
                initializeFailedMaterialField();
            }

            failedMaterial_()[cellI] = 1;
            failed_ = true;
            counter++;
        }
    }

    return failed_;
}


// ************************************************************************* //
