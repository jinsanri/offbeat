/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2012-2016 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "addToRunTimeSelectionTable.H"
#include "frictionalGapContactFvPatchVectorField.H"
#include "regionCoupledOFFBEATFvPatch.H"
#include "Time.H"
#include "fvc.H"
#include "AMIInterpolationOFFBEAT.H"
#include "PrimitivePatchInterpolation.H"
#include "volPointInterpolation.H"
#include "plane.H"
#include "line.H"
#include "gapGasModel.H"

// * * * * * * * * * * * * * Static Member Data  * * * * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Private members  * * * * * * * * * * * * *//
Foam::scalarField Foam::frictionalGapContactFvPatchVectorField::boundaryStiffness() const
{
    const regionCoupledOFFBEATFvPatch& patch
        = refCast<const regionCoupledOFFBEATFvPatch>(regionCoupledPatch_);

    const fvPatchField<scalar>& threeK = 
        patch.lookupPatchField<volScalarField, scalar>("threeK");
    
    scalarField K = 1.0/3.0*threeK;

    scalarField volumes(patch.size());
            
    forAll(volumes, cellI)
    {
            volumes[cellI] = patch.boundaryMesh().mesh().V()[patch.faceCells()[cellI]];
    }

    scalarField faceAreas(patch.size());
            
    forAll(faceAreas, cellI)
    {
            faceAreas[cellI] = patch.magSf()[cellI];
    }  

    return (K)*faceAreas/volumes;

}

Foam::tmp<Foam::scalarField> 
Foam::frictionalGapContactFvPatchVectorField::gapWidth() const
{
    const regionCoupledOFFBEATFvPatch& patch
        = refCast<const regionCoupledOFFBEATFvPatch>(regionCoupledPatch_);
    
    const regionCoupledOFFBEATFvPatch& nbrPatch
        = refCast<const regionCoupledOFFBEATFvPatch>(patch.nbrPatch());    

    const fvPatchVectorField& totalDispPatch = 
    patch.lookupPatchField<volVectorField, vector>("D"); 
      
    const fvPatchVectorField& totalDispNbrPatch = 
    nbrPatch.lookupPatchField<volVectorField, vector>("D"); 
        
    vectorField nf = patch.Sf() / patch.magSf();           
                   
    vectorField nbrNf = -regionCoupledPatch_.regionCoupledPatch().interpolate
                      (
                             nbrPatch.nf()
                      );    

    vectorField Cf = patch.Cf()
                   + totalDispPatch;                   
                   
    vectorField nbrCf = regionCoupledPatch_.regionCoupledPatch().interpolate
                      (
                             nbrPatch.Cf()
                             + totalDispNbrPatch
                      );

    return (nbrCf - Cf) & (nf+nbrNf)/mag(nf+nbrNf);
}    


void Foam::frictionalGapContactFvPatchVectorField::correctWeights() 
{
    // Correction factors for non matching surfaces (e.g. cylinders with a gap)
    scalarField correctionFactor(this->size(), 1);

    if (regionCoupledPatch_.owner())
    {
        const AMIInterpolationOFFBEAT& ami
            = regionCoupledPatch_.regionCoupledPatch().AMI();

        correctionFactor = ami.srcWeightsSum();
    }
    else
    {
        const AMIInterpolationOFFBEAT& ami
            = regionCoupledPatch_.nbrPatch().regionCoupledPatch().AMI();

        correctionFactor = ami.tgtWeightsSum();
    }

    // Reference to patch and neighbor patch
    const fvPatch& patch = this->patch();
    const fvPatch& nbrPatch = regionCoupledPatch_.neighbFvPatch();

    // Define normal vectors

    // Vector normal to this face
    vectorField ownN(patch.nf()); 

    // Vector normal to neighbour face (the opposite, with minus sign)
    vectorField nbrN(-regionCoupledPatch_.regionCoupledPatch().interpolate(
        regionCoupledPatch_.neighbFvPatch().nf())); 

    // Common normal direction (it corresponds to AMI seach direction)
    vectorField n(ownN+nbrN);
    n /= mag(n);

    vectorField nbrn(-nbrPatch.nf() + regionCoupledPatch_.nbrPatch().regionCoupledPatch().interpolate(ownN));

    //- NOTE: only taking pnf as a vectorField instead of patchNeighbourField
    //- works in multiprocessor 
    vectorField pnf = patchNeighbourField()();
    vectorField pf = patchInternalField()();

    // Displacement fields on this and neighbour patch
    const fvPatchVectorField& ownDf = 
    patch.lookupPatchField<volVectorField, vector>("D");
    const vectorField nbrDf = nbrPatch.lookupPatchField<volVectorField, vector>("D"); 
    const vectorField nbrDfInterp = regionCoupledPatch_.regionCoupledPatch().interpolate(
        nbrPatch.lookupPatchField<volVectorField, vector>("D"));

    const fvPatchVectorField& ownDfOld = 
    db().lookupObject<volVectorField>("D").oldTime().boundaryField()[patch.index()];       
    const vectorField nbrDfOldInterp = regionCoupledPatch_.regionCoupledPatch().interpolate(
     db().lookupObject<volVectorField>("D").oldTime().boundaryField()[nbrPatch.index()]); 

    // Patch face centers
    vectorField ownCf = 
    (
        patch.Cf() 
    ); 

    // Neighbour patch face centers
    vectorField nbrCf = 
    regionCoupledPatch_.regionCoupledPatch().interpolate
    (
         nbrPatch.Cf() 
    );

    // Cell centers
    const labelUList& ownFaceCells = patch.faceCells();
    vectorField ownCp(patch.boundaryMesh().mesh().C(), ownFaceCells);

    const labelUList& nbrFaceCells = nbrPatch.faceCells(); 
    vectorField nbrCellCenters(patch.boundaryMesh().mesh().C(), nbrFaceCells);
    vectorField nbrCp(regionCoupledPatch_.regionCoupledPatch().interpolate(
        nbrCellCenters));

    // Delta vectors
    vectorField ownDelta(ownCf - ownCp);

    // NOTE: nbrDelta is then adjusted to correct for the presence of the gap
    vectorField nbrDelta(nbrCp - (ownCf));

    forAll(*this, faceI)
    {
        if(nbrPatch.size() > 0)
        {
            if(mag(nbrN[faceI]) > SMALL)
            {

                plane facePlane(nbrCf[faceI], n[faceI]);
                scalar intersect(facePlane.normalIntersect(
                    ownCf[faceI], n[faceI]));

                vector projectedDist = intersect*n[faceI];

                if(intersect >= 0)
                {
                    nbrDelta[faceI] -= projectedDist; 
                }
            }
        }
    }

    // 1/deltaCoeff scalars
    ownDn = (ownN & (ownDelta));
    scalarField nbrDn
    (
        nbrN & (nbrDelta)
    );

    // Calculate two*Mu+Lambda (=K) 
    const fvPatchField<scalar>& ownMu =
        patch.lookupPatchField<volScalarField, scalar>("mu");
    const scalarField nbrMu
    (
        regionCoupledPatch_.regionCoupledPatch().interpolate
        (
            nbrPatch.lookupPatchField<volScalarField, scalar>("mu")
        )
    );

    const fvPatchField<scalar>& ownLambda =
        patch.lookupPatchField<volScalarField, scalar>("lambda");
    const scalarField nbrLambda
    (
        regionCoupledPatch_.regionCoupledPatch().interpolate
        (
            nbrPatch.lookupPatchField<volScalarField, scalar>("lambda")
        )
    );

    const scalarField ownK = (2*ownMu + ownLambda);
    const scalarField nbrK = (2*nbrMu + nbrLambda);

    // Take reference to grad patchField
    const tensorField ownGradD(patch.lookupPatchField<volTensorField, tensor>("gradD")); 
    const tensorField nbrGradD = 
    regionCoupledPatch_.regionCoupledPatch().interpolate
    (
         nbrPatch.lookupPatchField<volTensorField, tensor>("gradD")
    );         

    // Reference to stress fields
    const symmTensorField stress(patch.lookupPatchField<volSymmTensorField, symmTensor>("sigma")); 
    const symmTensorField nbrStress = 
    regionCoupledPatch_.regionCoupledPatch().interpolate
    (
        nbrPatch.lookupPatchField<volSymmTensorField, symmTensor>("sigma")
    );  

    // Non orthogonal corrections
    vectorField ownKcorr = 
    pos(ownN & ownDelta)*(ownN - (ownDelta/ownDn));
    vectorField nbrKcorr = 
    pos(nbrN & nbrDelta)*(nbrN - (nbrDelta/max(nbrDn,SMALL)));
    
    vectorField ownCorrectionN = (2.0*ownMu + ownLambda) * (( ownKcorr & (ownGradD & (n*n))) & (n*n));
    vectorField nbrCorrectionN = (2.0*nbrMu + nbrLambda) * (( nbrKcorr & (nbrGradD & (n*n))) & (n*n));

    vectorField ownCorrectionT = (ownMu) * (( ownKcorr & (ownGradD & (I - n*n))) & (I - n*n));
    vectorField nbrCorrectionT = (nbrMu) * (( nbrKcorr & (nbrGradD & (I - n*n))) & (I - n*n));

    //- Surface magnitudes
    const scalarField ownSf = patch.magSf();
    const scalarField nbrSf = 
    regionCoupledPatch_.regionCoupledPatch().interpolate
    (
        nbrPatch.magSf()
    ); 

    // Explicit components of the normal component of the traction
    vectorField ownQN = ((ownN & (stress)) & (n*n)) - 
    ownK*((ownN & ownGradD) & (n*n));
    
    vectorField nbrQN = ((nbrN & (nbrStress)) & (n*n)) - 
    nbrK*((nbrN & nbrGradD) & (n*n));    

    // Explicit components of the tangential component of the traction
    vectorField ownQT = ((ownN & (stress)) & (I - n*n)) - 
    ownMu*((ownN & ownGradD) & (I - n*n));
    
    vectorField nbrQT = ((nbrN & (nbrStress)) & (I - n*n)) - 
    nbrMu*((nbrN & nbrGradD) & (I - n*n));

    // Calculate initial gap to correct Cf position
    vectorField initialGap = (nbrCf - ownCf);

    // Correct for the original position of the center-faces   
    vectorField cfCorrection(this->size(), Foam::vector(0, 0, 0));
    forAll(*this, i)
    {
        cfCorrection[i] = 
        (nbrK[i]/max(nbrDn[i],SMALL)*( (initialGap[i]) & (n[i]*n[i]) )) +
        (nbrMu[i]/max(nbrDn[i],SMALL)*( (initialGap[i]) & (I - n[i]*n[i]) ));

        nbrQN[i] -= (cfCorrection[i] & (n[i]*n[i]));
        nbrQT[i] -= (cfCorrection[i] & (I - n[i]*n[i]));
    }

    // Correct for differences in surface coverage (e.g. cylindrical case)
    nbrQN *= correctionFactor;
    nbrQT *= correctionFactor;

    // Calculate normal "resistances"
    const scalarField ownRN = ownDn/(ownK);
    const scalarField nbrRN = nbrDn/max(nbrK,SMALL)/max(correctionFactor,SMALL);

    const scalarField RtotN = ownRN + nbrRN;

    //- Calculate tangent "resistances"
    const scalarField ownRT = ownDn/(ownMu);
    const scalarField nbrRT = nbrDn/max(nbrMu,SMALL)/max(correctionFactor,SMALL);

    const scalarField RtotT = ownRT + nbrRT;

    weightsN_ = (1 - ownRN/RtotN);
    weightsT_ = (1 - ownRT/RtotT);

    //- Patches dn to scale relative width
    scalar maxDn = min(gMin(ownDn), gMin(nbrPatch.nf() & nbrPatch.delta()));

    //- Sigmoid width parameter (relative width parameter times maxDn)
    scalar width = sigmoidWidth_*maxDn;
    scalar offset = sigmoidOffset_*maxDn;

    //- Argument multilpied and offset for exp() in blendCoeff
    scalar K = Foam::log(1e6 - 1)/(width);

    scalarField newBlendCoeff = neg(gapWidth_)*min
    (
        (1/(1 + exp(min( K*(max(gapWidth_, -maxDn) + offset), 500.0)))),
        1.0
    );

    blendCoeffs_ = relax_*newBlendCoeff + 
    (1 - relax_)*blendCoeffs_;

    // Attach if glued option is activated
    if(glued_)
    {
        blendCoeffs_ = 1;
    }

    forAll(*this, faceI)
    {
        if(nbrDn[faceI] > 0 and blendCoeffs_[faceI] > 0 )
        {         
            // Add non orthogonal correction to own and nbr source term
            // both for tangential and normal components
            ownQN[faceI] += ownCorrectionN[faceI];
            nbrQN[faceI] += nbrCorrectionN[faceI]*correctionFactor[faceI];
            ownQT[faceI] += ownCorrectionT[faceI];
            nbrQT[faceI] += nbrCorrectionT[faceI]*correctionFactor[faceI];

            // Set explicit components of traction vector

            // Explicit component of the normal traction
            vector explicitContactTractionN =                    
            (
                -ownK[faceI]*(1-weightsN_[faceI]) 
            )
            *(
                (pnf[faceI] & (I - sqr(n[faceI])))
                - (pf[faceI] & (I - sqr(n[faceI])))
            )/ownDn[faceI] 
            +
            (
                weightsN_[faceI]*(nbrQN[faceI])
                + (1 - weightsN_[faceI])*(ownQN[faceI])
            );

            // Tangential traction (fully explicit)
            vector contactTractionT =
            (
                ownMu[faceI]*(1-weightsT_[faceI])
                *(
                    (pnf[faceI] & (I - sqr(n[faceI])))
                    - (pf[faceI] & (I - sqr(n[faceI])))
                )/ownDn[faceI] +
                (
                    ownRT[faceI]/RtotT[faceI]*(ownQT[faceI]) +
                    nbrRT[faceI]/RtotT[faceI]*(nbrQT[faceI]) 
                )
            );

            // Total normal traction (explicit plus implicit parts)
            vector contactTractionNTotal = 
            explicitContactTractionN + 
            (ownK[faceI]*(1-weightsN_[faceI]))*
            (
                (pnf[faceI] & (sqr(n[faceI])))
                - (pf[faceI] & (sqr(n[faceI])))
            )/ownDn[faceI];

            // Apply friction limiter for tangential traction.
            // The resultant tangential traction is used as a fixed traction 
            scalar tangentTractionMag = min(
                mag(contactTractionT), 
                frictionCoeff_*mag(contactTractionNTotal));

            // Calculate 
            frictionToStickRatio_[faceI] = tangentTractionMag/max(
                mag(contactTractionT), SMALL);

            // Gradually increase tangent traction with penetration 
            // (i.e. tangent traction is zero when gap is open)
            contactTractionT *= frictionToStickRatio_[faceI];

            // Sum explicit components of the traction (normal and tangent)
            explicitTraction_[faceI] = 
            (
                blendCoeffs_[faceI]*explicitContactTractionN
                + blendCoeffs_[faceI]*contactTractionT
                + (1 - blendCoeffs_[faceI])*(-pressure_[faceI]*n[faceI])
            ); 
            
            // Remove the RhieChow correction from the system of equations
            explicitTraction_[faceI] += 
            (-(ownN[faceI] & stress[faceI]) + 
                ownK[faceI]*(ownN[faceI] & ownGradD[faceI]));

            // Set explicit components of boundary field value

            // Explicit component of the normal boundary displacement
            vector explicitValue_ContactN = 
            (explicitContactTractionN - ownQN[faceI])*ownDn[faceI]/ownK[faceI];
            
            // Explicit component of the boundary value of displacement due to gap
            vector explicitValue_PressureN = 
            ((-pressure_[faceI]*n[faceI]) - ownQN[faceI])*ownDn[faceI]/ownK[faceI];

            // Explicit tangential component of the boundary value of displacement
            // due to contact stresses
            vector explicitValue_ContactT =
            (contactTractionT - ownQT[faceI])*ownDn[faceI]/ownMu[faceI];   

            // Explicit tangential component of the boundary value of displacement
            // due to gap pressure (gap pressure tangent stress is zero)
            vector explicitValue_PressureT =
            (-ownQT[faceI])*ownDn[faceI]/ownMu[faceI];              

            // Total explicit component of the boundary value
            explicitValue_[faceI] = 
            (
                blendCoeffs_[faceI]*explicitValue_ContactN + 
                blendCoeffs_[faceI]*explicitValue_ContactT +
                (1 - blendCoeffs_[faceI])*explicitValue_PressureN +
                (1 - blendCoeffs_[faceI])*explicitValue_PressureT
            );  
        }
        else
        {
            weightsN_[faceI] = 1;
            weightsT_[faceI] = 1;

            blendCoeffs_[faceI] = 0;

            ownN[faceI] = patch.nf()()[faceI];

            explicitValue_[faceI] =
            (
                (
                    (- pressure_[faceI]*ownN[faceI]) - 
                    ( ownN[faceI] & stress[faceI] )
                )/ownK[faceI] 
                + ( ownN[faceI] & ownGradD[faceI] )
            )*ownDn[faceI];

            //- Discretize gradient on this face
            explicitTraction_[faceI] = explicitValue_[faceI]*(ownK[faceI]/ownDn[faceI]);

            vector ownCorrectionPressure =  (-ownKcorr[faceI] & ownGradD[faceI])*ownDn[faceI];

            explicitValue_[faceI] += (ownCorrectionPressure);
        }
    }               
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::frictionalGapContactFvPatchVectorField::
frictionalGapContactFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF
)
:
    coupledFvPatchField<vector>(p, iF),
    regionCoupledPatch_(refCast<const regionCoupledBaseOFFBEATFvPatch>(p)),
    coupled_(true),    
    weightsN_(patch().weights()),
    weightsT_(patch().weights()),
    blendCoeffs_(this->size(), 1),
    frictionToStickRatio_(this->size(), 0),
    gapWidth_(this->size(), 0),
    explicitValue_(this->size(), vector::zero),
    explicitTraction_(this->size(), vector::zero),
    ownDn(1/this->patch().deltaCoeffs()),
    pressure_(p.size(), 0.0),
    interfaceP_(p.size(), 0.0),
    relax_(1),
    stressName_("sigma"),
    glued_(false),
    sigmoidOffset_(0.0),
    sigmoidWidth_(0.1),
    frictionCoeff_(1)
{}


Foam::frictionalGapContactFvPatchVectorField::
frictionalGapContactFvPatchVectorField
(
    const frictionalGapContactFvPatchVectorField& rcpvf,
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    coupledFvPatchField<vector>(rcpvf, p, iF, mapper),
    regionCoupledPatch_(refCast<const regionCoupledBaseOFFBEATFvPatch>(p)),
    coupled_(rcpvf.coupled_),    
    weightsN_(patch().weights()), 
    weightsT_(patch().weights()),
    blendCoeffs_(rcpvf.blendCoeffs_),
    frictionToStickRatio_(rcpvf.frictionToStickRatio_),
    gapWidth_(this->size(), 0),
    explicitValue_(rcpvf.explicitValue_),
    explicitTraction_(rcpvf.explicitTraction_),
    ownDn(rcpvf.ownDn),
    pressure_(rcpvf.pressure_),
    interfaceP_(rcpvf.interfaceP_),
    relax_(rcpvf.relax_),
    stressName_(rcpvf.stressName_),
    glued_(rcpvf.glued_),
    sigmoidOffset_(rcpvf.sigmoidOffset_),
    sigmoidWidth_(rcpvf.sigmoidWidth_),
    frictionCoeff_(rcpvf.frictionCoeff_)
{}


Foam::frictionalGapContactFvPatchVectorField::
frictionalGapContactFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const dictionary& dict
)
:
    coupledFvPatchField<vector>(p, iF, dict),
    regionCoupledPatch_(refCast<const regionCoupledBaseOFFBEATFvPatch>(p)),
    coupled_(dict.lookupOrDefault<bool>("coupled", true)),
    weightsN_(patch().weights()),
    weightsT_(patch().weights()),
    blendCoeffs_(this->size(), 0),
    frictionToStickRatio_(this->size(), 0),
    gapWidth_(this->size(), 0),
    explicitValue_(this->size(), vector::zero),
    explicitTraction_(this->size(), vector::zero),
    ownDn(1/this->patch().deltaCoeffs()),
    pressure_(p.size(), 0.0),
    interfaceP_(this->size(), 0),
    relax_(dict.lookupOrDefault<scalar>("relax", 1)),
    stressName_(dict.lookupOrDefault<word>("stressName", "sigma")),
    glued_(dict.lookupOrDefault<bool>("glued", true)),
    sigmoidOffset_(dict.lookupOrDefault<scalar>("sigmoidOffset", 0.0)),
    sigmoidWidth_(dict.lookupOrDefault<scalar>("sigmoidWidth", 0.1)),
    frictionCoeff_(dict.lookupOrDefault<scalar>("frictionCoefficient", 0.0))
{    
    if (!isA<regionCoupledBaseOFFBEAT>(this->patch().patch()))
    {
        FatalErrorIn
        (
            "Foam::frictionalGapContactFvPatchVectorField::frictionalGapContactFvPatchVectorField"
            "("
            "    const fvPatch& p,"
            "    const DimensionedField<vector, volMesh>& iF,"
            "    const dictionary& dict"
            ")"
        )   << "' not type '" << regionCoupledBaseOFFBEAT::typeName << "'"
            << "\n    for patch " << p.name()
            << " of field " << internalField().name()
            << " in file " << internalField().objectPath()
            << exit(FatalError);
    }

    if (dict.found("pressure"))
    {
        pressure_ = scalarField("pressure", dict, patch().size());
    }     
}


Foam::frictionalGapContactFvPatchVectorField::
frictionalGapContactFvPatchVectorField
(
    const frictionalGapContactFvPatchVectorField& rcpvf,
    const DimensionedField<vector, volMesh>& iF
)
:
    coupledFvPatchField<vector>(rcpvf, iF),
    regionCoupledPatch_(rcpvf.regionCoupledPatch_),
    coupled_(rcpvf.coupled_),
    weightsN_(patch().weights()),
    weightsT_(patch().weights()),
    blendCoeffs_(rcpvf.blendCoeffs_),
    frictionToStickRatio_(rcpvf.frictionToStickRatio_),
    gapWidth_(this->size(), 0),
    explicitValue_(rcpvf.explicitValue_),
    explicitTraction_(rcpvf.explicitTraction_),
    ownDn(rcpvf.ownDn),
    pressure_(rcpvf.pressure_),
    interfaceP_(rcpvf.interfaceP_),
    relax_(rcpvf.relax_),
    stressName_(rcpvf.stressName_),
    glued_(rcpvf.glued_),
    sigmoidOffset_(rcpvf.sigmoidOffset_),
    sigmoidWidth_(rcpvf.sigmoidWidth_),
    frictionCoeff_(rcpvf.frictionCoeff_)
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::frictionalGapContactFvPatchVectorField::autoMap
(
    const fvPatchFieldMapper& m
)
{
    fvPatchVectorField::autoMap(m);
    m(weightsN_, weightsN_);
    m(gapWidth_, gapWidth_);
    m(pressure_, pressure_);
}


void Foam::frictionalGapContactFvPatchVectorField::rmap
(
    const fvPatchVectorField& ptf,
    const labelList& addr
)
{
    fvPatchVectorField::rmap(ptf, addr);

    const frictionalGapContactFvPatchVectorField& dmptf =
        refCast<const frictionalGapContactFvPatchVectorField>(ptf);

    weightsN_.rmap(dmptf.weightsN_, addr);
    gapWidth_.rmap(dmptf.gapWidth_, addr);
    pressure_.rmap(dmptf.pressure_, addr);
}


Foam::tmp<Foam::vectorField> Foam::frictionalGapContactFvPatchVectorField::
snGrad() const
{
    const fvPatch& patch = this->patch();

    // Lookup the gradient field
    const tensorField gradField =
        patch.lookupPatchField<volTensorField, tensor>
        (
            "grad" + internalField().name()
        );

    // Face unit normal
    const vectorField n = patch.nf();  

    const labelUList& ownFaceCells = patch.faceCells();
    vectorField ownCellCenters(patch.boundaryMesh().mesh().C(), ownFaceCells);
    vectorField ownCp(
                        ownCellCenters 
                    );

    // Delta vectors
    const vectorField delta = patch.Cf() - ownCp;

    // Non-orthogonal correction vectors
    vectorField k = delta - n*(n&delta);

    return (*this - (k&gradField) - patchInternalField())/ownDn;
}


Foam::tmp<Foam::vectorField> Foam::frictionalGapContactFvPatchVectorField::
snGrad(const scalarField&) const
{
    return snGrad();
}


void Foam::frictionalGapContactFvPatchVectorField::updateCoeffs()
{
    if (updated())
    {
        return;
    }

    // Update gap pressure
    pressure_ = db().lookupObject<gapGasModel>("gapGas").p();

    const regionCoupledOFFBEATFvPatch& patch
        = refCast<const regionCoupledOFFBEATFvPatch>(regionCoupledPatch_);
    
    const regionCoupledOFFBEATFvPatch& nbrPatch
        = refCast<const regionCoupledOFFBEATFvPatch>(patch.nbrPatch());

    const frictionalGapContactFvPatchVectorField& nbr = this->neighbour();
        
    gapWidth_ = patch.lookupPatchField<volScalarField, scalar>("gapWidth"); 

    //- Apply correction
    if (!regionCoupledPatch_.owner())
    {
        sigmoidOffset_ = nbr.sigmoidOffset_;
        frictionCoeff_ = nbr.frictionCoeff_;

        const AMIInterpolationOFFBEAT& ami(nbrPatch.regionCoupledPatch().AMI());

        // Update the gap width
        gapWidth_ = gapWidth();

        forAll(gapWidth_, faceI)
        {
            if(!ami.tgtAddress()[faceI].size())
            {
                gapWidth_[faceI] = GREAT;
            }
        }  
   
        forAll(gapWidth_, faceI)
        {
            if(!ami.tgtAddress()[faceI].size())
            {
                gapWidth_[faceI] = GREAT;
            }
        }

        // Debugging
        const_cast<fvPatchScalarField&>
        (
            patch.lookupPatchField<volScalarField, scalar>("gapWidth")
        ) = gapWidth_; 

        // The explicit terms are lumped together in Q
        const symmTensorField stress =
            patch.lookupPatchField<volSymmTensorField, symmTensor>("sigma"); 

        vectorField ownN(patch.nf()); 
        vectorField nbrN(-regionCoupledPatch_.regionCoupledPatch().interpolate(
            regionCoupledPatch_.neighbFvPatch().nf()));  

        vectorField n(ownN + nbrN);
        n /= mag(n);

        interfaceP_ = -( ((stress&ownN)&n) + pressure_ );
         
        forAll(interfaceP_, faceI)
        {
            if(gapWidth_[faceI] >= 0)
            {  
                interfaceP_[faceI]=  0;
            }
            else
            {
                interfaceP_[faceI]= max(interfaceP_[faceI], 0.0); 
            }
        }

        const_cast<fvPatchScalarField&>
        (
            patch.lookupPatchField<volScalarField, scalar>("interfaceP")
        ) = interfaceP_;
    }
    else 
    {
        const AMIInterpolationOFFBEAT& ami(regionCoupledPatch_.regionCoupledPatch().AMI());

        // Update the gap width
        gapWidth_ = gapWidth();

        forAll(gapWidth_, faceI)
        {
            if(!ami.tgtAddress()[faceI].size())
            {
                gapWidth_[faceI] = GREAT;
            }
        }    
   
        forAll(gapWidth_, faceI)
        {
            if(!ami.tgtAddress()[faceI].size())
            {
                gapWidth_[faceI] = GREAT;
            }
        }

        // Debugging
        const_cast<fvPatchScalarField&>
        (
            patch.lookupPatchField<volScalarField, scalar>("gapWidth")
        ) = gapWidth_; 


        // The explicit terms are lumped together in Q
        const symmTensorField stress =
            patch.lookupPatchField<volSymmTensorField, symmTensor>("sigma"); 

        vectorField ownN(patch.nf()); 
        vectorField nbrN(-regionCoupledPatch_.regionCoupledPatch().interpolate(
            regionCoupledPatch_.neighbFvPatch().nf()));  

        vectorField n(ownN + nbrN);
        n /= mag(n);

        interfaceP_ = -( ((stress&ownN)&n) + pressure_ );
         
        forAll(interfaceP_, faceI)
        {
            if(gapWidth_[faceI] >= 0)
            {  
                interfaceP_[faceI]=  0;
            }
            else
            {
                interfaceP_[faceI]= max(interfaceP_[faceI], 0.0); 
            }
        }

        const_cast<fvPatchScalarField&>
        (
            patch.lookupPatchField<volScalarField, scalar>("interfaceP")
        ) = interfaceP_;
    }

    correctWeights();

    fvPatchVectorField::updateCoeffs();
}


void Foam::frictionalGapContactFvPatchVectorField::evaluate
(
    const Pstream::commsTypes
)
{
   scalarField correctionFactor(this->size(), 1);

    if (regionCoupledPatch_.owner())
    {
        const AMIInterpolationOFFBEAT& ami
            = regionCoupledPatch_.regionCoupledPatch().AMI();

        correctionFactor = ami.srcWeightsSum();
    }
    else
    {
        const AMIInterpolationOFFBEAT& ami
            = regionCoupledPatch_.nbrPatch().regionCoupledPatch().AMI();

        correctionFactor = ami.tgtWeightsSum();
    }

    if (!updated())
    {
        updateCoeffs();
    }

    const fvPatch& patch = this->patch();    

    // Lookup the gradient field
    const tensorField gradField =
        patch.lookupPatchField<volTensorField, tensor>
        (
            "grad" + internalField().name()
        );
    
    vectorField::operator=
    (
        this->patchInternalField()
        -blendCoeffs_*(1.0 - weightsN_)*(this->patchInternalField())
        + blendCoeffs_*(1.0 - weightsN_)*(this->patchNeighbourField())
        + explicitValue_
    ); 

    fvPatchVectorField::evaluate();
}


Foam::tmp<Foam::Field<Foam::vector> >
Foam::frictionalGapContactFvPatchVectorField::
patchNeighbourField() const
{
    const fvPatch& nbrPatch = regionCoupledPatch_.neighbFvPatch();

    const labelUList& nbrFaceCells = nbrPatch.faceCells();
    
    const vectorField npf(internalField(), nbrFaceCells);

    return regionCoupledPatch_.regionCoupledPatch().interpolate(npf);
}

Foam::tmp<Foam::vectorField>
Foam::frictionalGapContactFvPatchVectorField::valueInternalCoeffs
(
    const tmp<scalarField>& w
) const
{
    Foam::vector ones(pTraits<vector>::one);

    tmp<vectorField> vICoeffs
    (
        new vectorField(this->size(), ones)
    ); 
    vectorField& vICoeffsRef = vICoeffs.ref();

    forAll(vICoeffsRef, i)
    {
        vICoeffsRef[i] *= weightsN_[i];
    } 

    return vICoeffs;
}


Foam::tmp<Foam::vectorField>
Foam::frictionalGapContactFvPatchVectorField::valueBoundaryCoeffs
(
    const tmp<scalarField>& w
) const
{
    Foam::vector ones(pTraits<vector>::one);

    tmp<vectorField> vBCoeffs
    (
        new vectorField(this->size(), ones)
    ); 
    vectorField& vBCoeffsRef = vBCoeffs.ref();

    forAll(vBCoeffsRef, i)
    {
        vBCoeffsRef[i] *= (1 - weightsN_[i]);
    } 

    return vBCoeffs;
}


Foam::tmp<Foam::vectorField>
Foam::frictionalGapContactFvPatchVectorField::gradientInternalCoeffs
(
    const scalarField&
) const
{
    return gradientInternalCoeffs();
}


Foam::tmp<Foam::vectorField>
Foam::frictionalGapContactFvPatchVectorField::gradientInternalCoeffs() const
{ 
    scalarField correctionFactor(this->size(), 1);

    if (regionCoupledPatch_.owner())
    {
        const AMIInterpolationOFFBEAT& ami
            = regionCoupledPatch_.regionCoupledPatch().AMI();

        correctionFactor = ami.srcWeightsSum();
    }
    else
    {
        const AMIInterpolationOFFBEAT& ami
            = regionCoupledPatch_.nbrPatch().regionCoupledPatch().AMI();

        correctionFactor = ami.tgtWeightsSum();
    }

    Foam::vector ones(pTraits<vector>::one);

    tmp<vectorField> gICoeffs
    (
        new vectorField(this->size(), ones)
    ); 
    vectorField& gICoeffsRef = gICoeffs.ref();

    const fvPatch& patch = this->patch(); 

    forAll(gICoeffsRef, i)
    {
        gICoeffsRef[i] *= 1*blendCoeffs_[i]*(weightsN_[i] - 1)/ownDn[i];
    };

    return gICoeffs;
}


Foam::tmp<Foam::vectorField>
Foam::frictionalGapContactFvPatchVectorField::gradientBoundaryCoeffs
(
    const scalarField& deltaCoeffs
) const
{
    return gradientBoundaryCoeffs();
}


Foam::tmp<Foam::vectorField>
Foam::frictionalGapContactFvPatchVectorField::gradientBoundaryCoeffs() const
{  
scalarField correctionFactor(this->size(), 1);

    if (regionCoupledPatch_.owner())
    {
        const AMIInterpolationOFFBEAT& ami
            = regionCoupledPatch_.regionCoupledPatch().AMI();

        correctionFactor = ami.srcWeightsSum();
    }
    else
    {
        const AMIInterpolationOFFBEAT& ami
            = regionCoupledPatch_.nbrPatch().regionCoupledPatch().AMI();

        correctionFactor = ami.tgtWeightsSum();
    }

    Foam::vector ones(pTraits<vector>::one);

    tmp<vectorField> gBCoeffs
    (
        new vectorField(this->size(), ones)
    ); 
    vectorField& gBCoeffsRef = gBCoeffs.ref();

    const fvPatch& patch = this->patch();
    symmTensorField valueFractionN(sqr(patch.nf()));
    symmTensorField valueFractionT(I - sqr(patch.nf()));

    forAll(gBCoeffsRef, i)
    {
        gBCoeffsRef[i] *= blendCoeffs_[i]*(1 - weightsN_[i])/ownDn[i];
    }

    return gBCoeffs;
}


void Foam::frictionalGapContactFvPatchVectorField::updateInterfaceMatrix
(
    scalarField& result,
    const scalarField& psiInternal,
    const scalarField& coeffs,
    const direction cmpt,
    const Pstream::commsTypes comms
) const
{
    const labelUList& faceCells = regionCoupledPatch_.faceCells();
    const labelUList& nbrFaceCells
        = regionCoupledPatch_.neighbFvPatch().faceCells();
    
    const scalarField pnf
        = regionCoupledPatch_.regionCoupledPatch().interpolate
        (
            scalarField(psiInternal, nbrFaceCells)
        );

    // Multiply the field by coefficients and add into the result
    forAll(faceCells, elemI)
    {
        result[faceCells[elemI]] -= coeffs[elemI]*pnf[elemI];
    }
    
}


void Foam::frictionalGapContactFvPatchVectorField::updateInterfaceMatrix
(
    Field<vector>& result,
    const Field<vector>& psiInternal,
    const scalarField& coeffs,
    const Pstream::commsTypes comms
) const
{
    const labelUList& faceCells = regionCoupledPatch_.faceCells();
    const labelUList& nbrFaceCells
        = regionCoupledPatch_.neighbFvPatch().faceCells();
    
    const vectorField pnf
        = regionCoupledPatch_.regionCoupledPatch().interpolate
        (
            vectorField(psiInternal, nbrFaceCells)
        );

    // Multiply the field by coefficients and add into the result
    forAll(faceCells, elemI)
    {
        result[faceCells[elemI]] -= coeffs[elemI]*pnf[elemI];
    }
}


void Foam::frictionalGapContactFvPatchVectorField::manipulateMatrix(fvMatrix<vector>& matrix)
{
    const fvPatch& patch = this->patch();
    const fvPatch& nbrPatch = regionCoupledPatch_.neighbFvPatch();

    const labelUList& faceCells = regionCoupledPatch_.faceCells();

    //- Create field to be added to the matrix source
    //- It is the explicit component of the Laplacian
    //- equal to the explicit component of the gradient
    // - multiplied by K and magnitude of face
    vectorField source(matrix.source().size(), Foam::vector(0, 0, 0));

    forAll(faceCells, elemI)
    {
        source[faceCells[elemI]] = (explicitTraction_[elemI])*patch.magSf()[elemI];
    }

    matrix.source() += source;
}


void Foam::frictionalGapContactFvPatchVectorField::write(Ostream& os) const
{
    fvPatchVectorField::write(os);

    writeEntryIfDifferent<scalar>(os, "relax", 1, relax_);
    writeEntryIfDifferent<bool>(os, "coupled", true, coupled_);
    writeEntryIfDifferent<bool>(os, "glued", false, glued_);
    writeEntryIfDifferent<word>(os, "stressName", "sigma", stressName_);
    
    if (regionCoupledPatch_.owner()) 
    {    
        writeEntryIfDifferent<scalar>(os, "sigmoidOffset", 0.0, sigmoidOffset_);
        writeEntryIfDifferent<scalar>(os, "sigmoidWidth", 0.1, sigmoidWidth_);
        writeEntryIfDifferent<scalar>(os, "frictionCoefficient", 0.0, frictionCoeff_);
    }

    writeEntry(os, "pressure", pressure_);
    writeEntry(os, "value", *this);
}



// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
    makePatchTypeField
    (
        fvPatchVectorField,
        frictionalGapContactFvPatchVectorField
    );
};


// ************************************************************************* //
