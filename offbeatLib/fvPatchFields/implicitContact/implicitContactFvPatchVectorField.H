/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2012-2016 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::implicitContactFvPatchVectorField

Description
    General region-coupled patchField.
    Conceptually similar to cyclicAMI, but does not assume continuity
    across the interface, i.e. the fvPatch is treated as uncoupled
    from the delta point of view. This allows for exchange of different
    physics across the interface.
    If the patchField is defined as coupled, the fvPatch is an interface
    and is incorporated into the matrix implicitly.
    If not coupled, then the patchField reverts to a simple
    zeroGradient.

\todo
    To be moved to a coupled patch with correct weight.

SourceFiles
    implicitContactFvPatchVectorField.C

\*---------------------------------------------------------------------------*/

#ifndef implicitContactFvPatchVectorField_H
#define implicitContactFvPatchVectorField_H

#include "regionCoupledBaseOFFBEATFvPatch.H"
#include "LduInterfaceField.H"
#include "fvPatchField.H"
#include "coupledFvPatchField.H"
#include "volFields.H"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
             Class implicitContactFvPatchVectorField Declaration
\*---------------------------------------------------------------------------*/

class implicitContactFvPatchVectorField
:
    public coupledFvPatchField<vector>
{

protected:

    // Private data

        //- Local reference to region couple patch
        const regionCoupledBaseOFFBEATFvPatch& regionCoupledPatch_;

        //- True if patch is coupled. False reverts to zeroGradient field.
        bool coupled_;
        
        //- Coupled patch weights for normal component
        mutable scalarField weightsN_;
        
        //- Coupled patch weights for tangential component
        mutable scalarField weightsT_;
        
        //- Blending coefficients
        mutable scalarField blendCoeffs_;
        
        //- Gap widht
        mutable scalarField gapWidth_;
        
        //- Initial gap widht
        mutable scalarField initialGap;
        
        //- Boundary coefficient
        mutable scalarField twoMuLambda_;
        
        //- Explicit component of the boudary displacement
        mutable vectorField explicitValue;
        
        //- Explicit component of traction
        mutable vectorField explicitTraction;
        
        //- NOrmal gradient
        mutable vectorField snGrad_;     
        
        //- Normals on this patch
        mutable vectorField ownN;
        
        //- Normals on neighbour patch
        mutable vectorField nNbr;
        
        //- Face center to cell center distance
        mutable scalarField ownDn;
        
        // Surface pressure
        scalarField pressure_;
        
        // Interfacial Pressure
        mutable scalarField interfaceP_;
        
        // Relaxation factor for gradient update
        scalar relax_;
        
        //- Width of exponential corresponding to a value of 1e-6
        scalar width1e6_;
        
        //- Value of exponential offset
        scalar offset_;

        //- If true, width1e6_ is a fraction of the cell-center to face distance.
        //- If false, width1e6_ is an absolute distance from the surface, in m.
        Switch relativeWidth_;

        //- Truncates exponential outside of penetration zone.
        //- It enforces no positve normal traction, but convergence is more difficult.
        Switch truncate_;
        
        //- Friction Coefficient (between 0 and 1)
        scalar friction_;

        //- Name of stress field
        word stressName_;    

        //- Treat surfaces as attached
        Switch attached_;

        //- Update AMI mapping?
        Switch updateAMI_;

    // Private functions

        //- Correct weights for this coupled field
        virtual void correctWeights();

        //- Return the gap width
        tmp<scalarField> gapWidth() const;

        //- Return the neighbour patchField
        inline const implicitContactFvPatchVectorField& neighbour() const
        {
            return refCast<const implicitContactFvPatchVectorField>
            (
                regionCoupledPatch_.neighbFvPatch().lookupPatchField
                <volVectorField, vector>
                (
                    internalField().name()
                )
            );
        }

        //- Minimum patch stiffness for interface pressure evaluation
        scalar boundaryStiffness() const;


public:

    //- Runtime type information
    TypeName("implicitContact");


    // Constructors

        //- Construct from patch and internal field
        implicitContactFvPatchVectorField
        (
            const fvPatch&,
            const DimensionedField<vector, volMesh>&
        );

        //- Construct from patch, internal field and dictionary
        implicitContactFvPatchVectorField
        (
            const fvPatch&,
            const DimensionedField<vector, volMesh>&,
            const dictionary&
        );

        //- Construct by mapping given implicitContactFvPatchVectorField
        // onto a new patch
        implicitContactFvPatchVectorField
        (
            const implicitContactFvPatchVectorField&,
            const fvPatch&,
            const DimensionedField<vector, volMesh>&,
            const fvPatchFieldMapper&
        );


        //- Construct as copy setting internal field reference
        implicitContactFvPatchVectorField
        (
            const implicitContactFvPatchVectorField&,
            const DimensionedField<vector, volMesh>&
        );

        //- Construct and return a clone setting internal field reference
        virtual tmp<fvPatchField<vector> > clone
        (
            const DimensionedField<vector, volMesh>& iF
        ) const
        {
            return tmp<fvPatchField<vector> >
            (
                new implicitContactFvPatchVectorField(*this, iF)
            );
        }


    //- Destructor
    virtual ~implicitContactFvPatchVectorField()
    {}


    // Member functions

        // Access
        
            //- Interface pressure
            inline const scalarField& interfaceP() const
            {
                return interfaceP_;
            } 
            
        // Mapping functions

            //- Map (and resize as needed) from self given a mapping object
            virtual void autoMap
            (
                const fvPatchFieldMapper&
            );

            //- Reverse map the given fvPatchField onto this fvPatchField
            virtual void rmap
            (
                const fvPatchVectorField&,
                const labelList&
            );
            
        // Evaluation functions

            //- Return neighbour coupled internal cell data
            virtual tmp<vectorField> patchNeighbourField() const;

            //- Update the coefficients associated with the patch field
            //  Sets Updated to true
            virtual void updateCoeffs();
            
            //- Evaluate the patch field
            virtual void evaluate
            (
                const Pstream::commsTypes commsType
            );

            //- Return patch-normal gradient
            virtual tmp<vectorField> snGrad() const;

            //- Return patch-normal gradient
            //  Note: the deltaCoeffs supplied are not used
            virtual tmp<vectorField> snGrad
            (
                const scalarField& deltaCoeffs
            ) const;
            
            //- Return the matrix diagonal coefficients corresponding to the
            //  evaluation of the value of this patchField with given weights
            virtual tmp<vectorField> valueInternalCoeffs
            (
                const tmp<scalarField>&
            ) const;

            //- Return the matrix source coefficients corresponding to the
            //  evaluation of the value of this patchField with given weights
            virtual tmp<vectorField> valueBoundaryCoeffs
            (
                const tmp<scalarField>&
            ) const;

            //- Return the matrix diagonal coefficients corresponding to the
            //  evaluation of the gradient of this patchField
            virtual tmp<vectorField> gradientInternalCoeffs
            (
                const scalarField& deltaCoeffs
            ) const;

            //- Return the matrix diagonal coefficients corresponding to the
            //  evaluation of the gradient of this patchField
            virtual tmp<vectorField> gradientInternalCoeffs() const;

            //- Return the matrix source coefficients corresponding to the
            //  evaluation of the gradient of this patchField
            virtual tmp<vectorField> gradientBoundaryCoeffs
            (
                const scalarField& deltaCoeffs
            ) const;

            //- Return the matrix source coefficients corresponding to the
            //  evaluation of the gradient of this patchField
            virtual tmp<vectorField> gradientBoundaryCoeffs() const;
            
        // Coupled interface functionality

            //- Update result field based on interface functionality
            virtual void updateInterfaceMatrix
            (
                scalarField& result,
                const scalarField& psiInternal,
                const scalarField& coeffs,
                const direction cmpt,
                const Pstream::commsTypes commsType
            ) const;

            //- Update result field based on interface functionality
            virtual void updateInterfaceMatrix
            (
                Field<vector>&,
                const Field<vector>&,
                const scalarField&,
                const Pstream::commsTypes commsType
            ) const;  

            virtual void manipulateMatrix(fvMatrix<vector>& matrix);      

            //- Return the interface type
            virtual const word& interfaceFieldType() const
            {
                return regionCoupledPatch_.regionCoupleType();
            }

             virtual bool coupled() const
             {
                 return coupled_;
             }


        //- Write
        virtual void write(Ostream&) const;
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
