/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "gapContactFvPatchVectorField.H"
#include "addToRunTimeSelectionTable.H"
#include "volFields.H"
#include "regionCoupledOFFBEATFvPatch.H"
#include "AMIInterpolationOFFBEAT.H"
#include "PrimitivePatchInterpolation.H"
#include "volPointInterpolation.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * Static Member Data  * * * * * * * * * * * * * * //


// * * * * * * * * * * * * * * * * Private members  * * * * * * * * * * * * *//
    
Foam::tmp<Foam::scalarField> 
Foam::gapContactFvPatchVectorField::gapWidth() const
{
    const regionCoupledOFFBEATFvPatch& patch
        = refCast<const regionCoupledOFFBEATFvPatch>(regionCoupledPatch_);
    
    const regionCoupledOFFBEATFvPatch& nbrPatch
        = refCast<const regionCoupledOFFBEATFvPatch>(patch.nbrPatch());    

    const fvPatchVectorField& totalDispPatch = 
    ( 
        patch.boundaryMesh().mesh().foundObject<fvMesh>("referenceMesh")
    ) ?
    patch.lookupPatchField<volVectorField, vector>("DD")
    : 
    patch.lookupPatchField<volVectorField, vector>("D"); 

    const fvPatchVectorField& totalDispNbrPatch = 
    ( 
        patch.boundaryMesh().mesh().foundObject<fvMesh>("referenceMesh")
    ) ?
    nbrPatch.lookupPatchField<volVectorField, vector>("DD")
    :
    nbrPatch.lookupPatchField<volVectorField, vector>("D");

    vectorField nf = patch.Sf() / patch.magSf();

    vectorField Cf = patch.Cf() + totalDispPatch;                   
                   
    vectorField nbrCf = regionCoupledPatch_.regionCoupledPatch().interpolate
                      (
                             nbrPatch.Cf() + totalDispNbrPatch
                      );

    return (nbrCf - Cf) & nf;
}


Foam::scalarField gapContactFvPatchVectorField::boundaryStiffness() const
{
    const regionCoupledOFFBEATFvPatch& patch
        = refCast<const regionCoupledOFFBEATFvPatch>(regionCoupledPatch_);

    const fvPatchField<scalar>& threeK = 
        patch.lookupPatchField<volScalarField, scalar>("threeK");
    
    scalarField K = 1.0/3.0*threeK;

    scalarField volumes(patch.size());
            
    forAll(volumes, cellI)
    {
            volumes[cellI] = patch.boundaryMesh().mesh().V()[patch.faceCells()[cellI]];
    }

    scalarField faceAreas(patch.size());
            
    forAll(faceAreas, cellI)
    {
            faceAreas[cellI] = patch.magSf()[cellI];
    }  

    return (K)*faceAreas/volumes;

}

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

gapContactFvPatchVectorField::gapContactFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF
)
:
    tractionDisplacementFvPatchVectorField(p, iF),
    regionCoupledPatch_(refCast<const regionCoupledBaseOFFBEATFvPatch>(p)),
    penaltyScaleFact_(0.1),
    frictionCoeff_(0.0),
    gapWidth_(p.size(), 0),
    interfaceP_(p.size(), 0),
    slip_(p.size(), Foam::vector::zero),
    frictionTraction_(p.size(), Foam::vector::zero),
    relaxInterP_(1)
{}


gapContactFvPatchVectorField::gapContactFvPatchVectorField
(
    const gapContactFvPatchVectorField& ptf,
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    tractionDisplacementFvPatchVectorField(ptf, p, iF, mapper),
    regionCoupledPatch_(refCast<const regionCoupledBaseOFFBEATFvPatch>(p)),
    penaltyScaleFact_(ptf.penaltyScaleFact_),
    frictionCoeff_(ptf.frictionCoeff_),
    gapWidth_(ptf.gapWidth_),
    interfaceP_(ptf.interfaceP_),
    slip_(ptf.slip_),
    frictionTraction_(ptf.frictionTraction_),
    relaxInterP_(ptf.relaxInterP_)
{}


gapContactFvPatchVectorField::gapContactFvPatchVectorField
(
    const fvPatch& p,
    const DimensionedField<vector, volMesh>& iF,
    const dictionary& dict
)
:
    tractionDisplacementFvPatchVectorField(p, iF, dict, false),
    regionCoupledPatch_(refCast<const regionCoupledBaseOFFBEATFvPatch>(p)),
    penaltyScaleFact_(dict.lookupOrDefault<scalar>("penaltyFactor", 0.1)),
    frictionCoeff_(dict.lookupOrDefault<scalar>("frictionCoefficient", 0.0)),
    gapWidth_(p.size(), 0),
    interfaceP_(p.size(), 0),
    slip_(p.size(), Foam::vector::zero),
    frictionTraction_(p.size(), Foam::vector::zero),
    relaxInterP_(dict.lookupOrDefault<scalar>("relaxInterfacePressure", 1))
{    
    if (dict.found("interfaceP"))
    {
        interfaceP_ = scalarField("interfaceP", dict, p.size());
    }

    if (dict.found("slip"))
    {
        slip_ = vectorField("slip", dict, p.size());
    }

    if (dict.found("frictionTraction"))
    {
        frictionTraction_ = vectorField("frictionTraction", dict, p.size());
    }
}


gapContactFvPatchVectorField::gapContactFvPatchVectorField
(
    const gapContactFvPatchVectorField& ptf,
    const DimensionedField<vector, volMesh>& iF
)
:
    tractionDisplacementFvPatchVectorField(ptf, iF),
    regionCoupledPatch_(ptf.regionCoupledPatch_),
    penaltyScaleFact_(ptf.penaltyScaleFact_),
    frictionCoeff_(ptf.frictionCoeff_),
    gapWidth_(ptf.gapWidth_),
    interfaceP_(ptf.interfaceP_),
    slip_(ptf.slip_),
    frictionTraction_(ptf.frictionTraction_),
    relaxInterP_(ptf.relaxInterP_)
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //


void gapContactFvPatchVectorField::autoMap
(
    const fvPatchFieldMapper& m
)
{
    tractionDisplacementFvPatchVectorField::autoMap(m);
    m(gapWidth_, gapWidth_);
    m(interfaceP_, interfaceP_);
}


void gapContactFvPatchVectorField::rmap
(
    const fvPatchVectorField& ptf,
    const labelList& addr
)
{
    tractionDisplacementFvPatchVectorField::rmap(ptf, addr);

    const gapContactFvPatchVectorField& dmptf =
        refCast<const gapContactFvPatchVectorField>(ptf);

    gapWidth_.rmap(dmptf.gapWidth_, addr);
    interfaceP_.rmap(dmptf.interfaceP_, addr);
}

void gapContactFvPatchVectorField::updateCoeffs()
{
    if (updated())
    {
        return;
    }
    
    const regionCoupledOFFBEATFvPatch& patch
        = refCast<const regionCoupledOFFBEATFvPatch>(regionCoupledPatch_);
    
    const regionCoupledOFFBEATFvPatch& nbrPatch
        = refCast<const regionCoupledOFFBEATFvPatch>(patch.nbrPatch());

    const gapContactFvPatchVectorField& nbr = this->neighbour();

    scalarField correctionFactor(patch.size(), 1);

    if (!regionCoupledPatch_.owner() )
    {
        penaltyScaleFact_ = nbr.penaltyScaleFact_;
        frictionCoeff_ = nbr.frictionCoeff_;

        const AMIInterpolationOFFBEAT& ami(nbrPatch.regionCoupledPatch().AMI());
        
        scalarField oldGapWidth = gapWidth_;

        // Update the gap width
        gapWidth_ = 
        // 0.5*
        (
            gapWidth()
          // + patch.regionCoupledPatch().interpolate(nbr.gapWidth())
        );

        // Prepare slave and master DD fields for slip/friction calculation
        vectorField ownDD(patch.size(), Foam::vector::zero);
        vectorField nbrDD(patch.size(), Foam::vector::zero);

        if(db().foundObject<fvMesh>("referenceMesh"))
        {
            const volVectorField& DD =
                db().lookupObject<volVectorField>("DD");

            ownDD = DD.boundaryField()[patch.index()];
            nbrDD = patch.regionCoupledPatch().interpolate(
                DD.boundaryField()[nbrPatch.index()]);
        }
        else
        {
            const volVectorField& D =
                db().lookupObject<volVectorField>("D");

            ownDD = D.boundaryField()[patch.index()] - 
            D.oldTime().boundaryField()[patch.index()];

            nbrDD = patch.regionCoupledPatch().interpolate(
                D.boundaryField()[nbrPatch.index()] -
                D.oldTime().boundaryField()[nbrPatch.index()]);            
        }

        // forAll(gapWidth_, faceI)
        // {
        //     if(!ami.tgtAddress()[faceI].size())
        //     {
        //         gapWidth_[faceI] = GREAT;
        //     }
        // }
        
        // Update the neighbour gap width
        nbr.gapWidth_ = nbrPatch.regionCoupledPatch().interpolate(gapWidth_);     

        // Debugging
        const_cast<fvPatchScalarField&>
        (
            patch.lookupPatchField<volScalarField, scalar>("gapWidth")
        ) = gapWidth_;   

        // Debugging
        const_cast<fvPatchScalarField&>
        (
            nbrPatch.lookupPatchField<volScalarField, scalar>("gapWidth")
        ) = nbr.gapWidth_;    

        // Interpolate back to this patch, so to avoid scalarFields of different lenght
        const scalarField nbrBoundaryStiff(patch.regionCoupledPatch().interpolate(nbr.boundaryStiffness()));

        scalarField interfacePOld = interfaceP_;

        scalarField penaltyFact = penaltyScaleFact_*min(
            boundaryStiffness(), nbrBoundaryStiff);
        
        // Calculate the interface pressure
        interfaceP_ = max
        (
           -penaltyFact*gapWidth_,
            0.0
        );

        // Calculate slip vector (only if gapWidth is negative, otherwise 0)
        vectorField slipOld(slip_);
        slip_ = neg(gapWidth_)*(ownDD - nbrDD);
        slip_ = (I - sqr(patch.nf())) & slip_;
        slip_ = relaxInterP_*slip_ + (1 - relaxInterP_)*slipOld;

        // Calculate friction traction magnitude and relax
        scalarField frictionMag(min(
            penaltyFact*mag(slip_), frictionCoeff_*interfaceP_));

        frictionMag = relaxInterP_*frictionMag + (1 - relaxInterP_)*mag(frictionTraction_);

        // Calculate friction traction vector
        frictionTraction_ = frictionMag*(-slip_/max(mag(slip_), VSMALL));

        // Interpolate contact pressure on the master side
        interfaceP_ = (relaxInterP_*interfaceP_ + (1 - relaxInterP_)*interfacePOld);

        nbr.interfaceP_ = nbrPatch.regionCoupledPatch().interpolate(interfaceP_);

        const_cast<fvPatchScalarField&>
        (
            patch.lookupPatchField<volScalarField, scalar>("interfaceP")
        ) = interfaceP_; 

        const_cast<fvPatchScalarField&>
        (
            nbrPatch.lookupPatchField<volScalarField, scalar>("interfaceP")
        ) = nbr.interfaceP_;  

        // Interpolate slip and friction on the master side (change sign)
        nbr.slip_ = -nbrPatch.regionCoupledPatch().interpolate(slip_);
        nbr.frictionTraction_ = -nbrPatch.regionCoupledPatch().interpolate(
            frictionTraction_);

        // Update correction factors
        correctionFactor = ami.tgtWeightsSum();
    }
    
    // Include the gap gas pressure
    const gapGasModel& gapGas
    = patch.boundaryMesh().mesh().lookupObject<gapGasModel>("gapGas");

    vectorField n(patch.nf());

    pressure_ = gapGas.p()  + interfaceP_*correctionFactor;
    traction_ = frictionTraction_;

    tractionDisplacementFvPatchVectorField::updateCoeffs();
}


void gapContactFvPatchVectorField::write(Ostream& os) const
{
    tractionDisplacementFvPatchVectorField::write(os);
    
    if (regionCoupledPatch_.owner()) 
    {    
        writeEntryIfDifferent<scalar>(os, "penaltyFactor", 0.1, penaltyScaleFact_);
        writeEntryIfDifferent<scalar>(os, "frictionCoefficient", 0.0, frictionCoeff_);
    }

    writeEntryIfDifferent<scalar>(os, "relaxInterfacePressure", 1, relaxInterP_);
    
    writeEntry(os, "interfaceP", interfaceP_);
    writeEntry(os, "slip", slip_);
    writeEntry(os, "frictionTraction", frictionTraction_);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

makePatchTypeField
(
    fvPatchVectorField,
    gapContactFvPatchVectorField
);

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
