/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::lhgr

Description
    Derived from the base heatSource class, this class act as the parent class 
    for all heat source models that define the linear heat generation rate (lhgr)
    as an additional field. 

    The parent class typeName is "lhgr". If selected in the solverDict
    the heat source model creates the field 'lhgr' together with 'Q'. 

    If the 'lhgr' file is present in the starting time folder, the internal field 
    and boundary conditions are set according to the file. 

    Otherwise, the field is set to 0 W/m in each cell, with zeroGradient BCs 
    in all non-empty/-wedge patches.

Usage
    In solverDict file:
    \verbatim
    heatSource lhgr;
    \endverbatim
    
SourceFiles
    heatSource.C

\todo
    - The lhgr might be needed by some models such as the relocation, therefore
    it might be useful to have it as a field in the registry. Alternatively, 
    one could make the heatSource class an IOObject and ask directly for the lhgr 
    with a function. In this case having the lhgr as a vol field might not be necessary.

\mainauthor
    A. Scolaro - EPFL (ECOLE POLYTECHNIQUE FEDERALE DE LAUSANNE, Switzerland
    Laboratory for Reactor Physics and Systems Behaviour)

\contribution
    I. Clifford - PSI (Paul Scherrer Institut, Switzerland)\n
    E. Brunetto, C. Fiorina - EPFL

\date 
    November 2021

\*---------------------------------------------------------------------------*/

#ifndef lhgr_H
#define lhgr_H

#include "heatSource.H"
#include "axialProfile.H"
#include "radialProfile.H"
#include "volFields.H"
#include "InterpolateTables.H"
#include "globalOptions.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

/*---------------------------------------------------------------------------*\
                         Class lhgr Declaration
\*---------------------------------------------------------------------------*/

class lhgr
:
    public heatSource
{
 
    // Private data
    
    // Private Member Functions
        
        //- Disallow default bitwise copy construct
        lhgr(const lhgr&);

        //- Disallow default bitwise assignment
        void operator=(const lhgr&);
        
protected:   

    // Protected data
        
        //- Linear heat generation rate (W/m)
        volScalarField lhgr_;

public:

    //- Runtime type information
    TypeName("lhgr");

    // Constructors

        //- Construct from mesh, materials and dict
        lhgr
        (
            const fvMesh& mesh,
            const materials& materials,
            const dictionary& heatSourceOptDict
        );


    //- Destructor
    ~lhgr();


    // Member Functions
    
        //- update the power distribution in the supplied list of cells
        virtual void correct();
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
