/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    offbeat

Description
    OFFBEAT (OpenFOAM Fuel BEhavior Analysis Tool) is a multi-dimensional fuel 
    performance code based on the open-source C++ library OpenFOAM. It can be
    used both for the analysis of standard 1.5D cases and for more complex 2D 
    and 3D local effects.

    TODO list:
    [-] Include fgr in gapGas->correctMass
    [-] Verify that 2 min outerIter are necessary
    [-] Description in file header
    [-] Eliminate writeStressField?

\*---------------------------------------------------------------------------*/

// Include standard classes
#include "fvCFD.H"
#include "SortableList.H"

// Include main OFFBEAT classes
#include "globalFieldLists.H"
#include "globalOptions.H"
#include "offbeatTime.H"
#include "thermalSubSolver.H"
#include "mechanicsSubSolver.H"
#include "gapGasModel.H"
#include "fissionGasRelease.H"
#include "burnup.H"
#include "neutronicsSubSolver.H"
#include "heatSource.H"
#include "fastFlux.H"
#include "timeHandling.H"
#include "sliceMapper.H"
#include "materials.H"
#include "rheology.H"
#include "corrosion.H"
#include "userParameters.H"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"

    Info<< "Create time\n" << Foam::endl;
  
    offbeatTime runTime
    (
        Foam::Time::controlDictName, 
        args
    );
 
#   include "createMesh.H"

    // Read main OFFBEAT dictionary
    IOdictionary solverDict
    (
        IOobject
        (
            "solverDict",
            mesh.time().constant(),
            mesh,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

    // Create objects and autoPtr to main OFFBEAT classes
    timeHandling adjustableTime(mesh, runTime);
    globalOptions globalOpt_(mesh, solverDict);
    autoPtr<materials> mat_(materials::New(mesh, solverDict));
    
    autoPtr<sliceMapper> mapper_(sliceMapper::New(mesh, mat_, solverDict));

    autoPtr<rheology> rheo_(rheology::New(mesh, mat_, solverDict));    
    autoPtr<heatSource> heatSrc_(heatSource::New(mesh, mat_, solverDict));
    autoPtr<burnup> burnup_(burnup::New(mesh, mat_, solverDict));
    
    autoPtr<neutronicsSubSolver> neutronics_
    (neutronicsSubSolver::New(mesh, burnup_, solverDict));
    
    autoPtr<fastFlux> fastFlux_(fastFlux::New(mesh, mat_, solverDict));
    
    autoPtr<thermalSubSolver> thermal_
    (thermalSubSolver::New(mesh, mat_(), solverDict));
    
    autoPtr<mechanicsSubSolver> mechanics_
    (mechanicsSubSolver::New(mesh, mat_, rheo_(), solverDict));
    
    autoPtr<fissionGasRelease> fgr_
    (fissionGasRelease::New(mesh, mat_, solverDict)); 
    
    autoPtr<gapGasModel> gapGas_
    (gapGasModel::New
        (mesh, mat_, thermal_->T(), mechanics_->DorDD(), fgr_,  solverDict)); 

    autoPtr<corrosion> corrosion_
    (corrosion::New(mesh, mat_, solverDict)); 
    

    // Check user parameters
    listUserParameters();
    listRegisteredUserParameters();
    checkUserParameters();
    
    // Keep track of total outer iterations from start to end
    scalar totalIterations(0);

    adjustableTime.setLastTimeMarker
    (
        min
        (
            heatSrc_->lastTimeMarker(),
            runTime.endTime().value()
        )
    );

    // Start time-loop
    while (runTime.run())
    {
        // True when time step is converged
        bool converged(false);

        // Read from fvSolution dict the maximum number of outer iterations
        const dictionary& stressControl = 
        mesh.solutionDict().subDict("stressAnalysis");
        int maxOuterIter(readInt(stressControl.lookup("maxOuterIter")));

        runTime ++;

        Info<< "Time = " << runTime.userTime() << runTime.unit() << nl << endl;

        // Track number of outer iterations (per time-step)
        int nOuterIter = 0;

        //- Update mesh if necessary
        if
        (
            mesh.foundObject<fvMesh>("referenceMesh")
        )
        {
            mechanics_->updateMesh();
        }
        
        // Update incremental fields (DD and gradDD = 0) if necessary
        mechanics_->updateIncrementalFields();

        do 
        {
            Info << "OuterIteration n. " << nOuterIter << nl <<  endl;

            storeGlobalFieldsPrevIter();

            // First update corrosion, only then update AMI. Necessary because
            // if corrosion moves the mesh, the AMI is also updated given that 
            // moving the mesh clears the geometry and the AMIPtr. However this
            // first update, does not use the updated mesh location
            // TODO: find a way to update mesh without AMI?
            corrosion_->correct();

            forAll(mesh.boundary(), patchi)
            {
                if(isType<regionCoupledOFFBEATFvPatch>(mesh.boundary()[patchi]))
                {
                   const regionCoupledOFFBEATFvPatch& patch
                    = refCast<const regionCoupledOFFBEATFvPatch>(mesh.boundary()[patchi]);

                    patch.updateAMI();
                }
            }


            fgr_->correct();
            gapGas_->correct();
            heatSrc_->correct();
            neutronics_->correct();
            burnup_->correct();
            fastFlux_->correct();
            thermal_->correct();
            mat_->correctBehavioralModels();
            rheo_->getAdditionalStrain();
            mechanics_->correct();

            correctBCsGlobalFields();
            relaxGlobalFields();

            nOuterIter++;
            totalIterations++;
            
            // Check convergence
            converged = 
            (
                mechanics_->converged() 
                and 
                thermal_->converged()
                and 
                rheo_->converged()
                and
                neutronics_->converged()
            );
        }
        while
        (
           (nOuterIter < 2)  
           || 
           (not(converged) && nOuterIter < maxOuterIter) 
        );

        mechanics_->updateTotalFields();
        gapGas_->updateVariables();    
        fgr_->updateVariables();

        // Write fields on disk if this is a write time
        if(adjustableTime.write())
        {
            if (runTime.outputTime())
            {  
                mechanics_->writeStressFields();    
            }
            
            runTime.write();
        }

        //- Check if failure occurred
        mat_->checkFailure();
        
        Info<< "ExecutionTime = " << runTime.elapsedCpuTime() << " s"
            << "  ClockTime = " << runTime.elapsedClockTime() << " s"
            << "  TotalIterations = " << totalIterations
            << nl << endl;  

        // Check if runTime is below maximum.
        if(runTime.value() >= heatSrc_->lastTimeMarker())
        {
            break;
        } 

        // TODO could we have the deltaT setting inside the time handling class?
        // Set the deltaT based on burnup and power
        // if(runTime.timeIndex() > 0)
        {   
            scalar deltaT(GREAT);
            deltaT = min(deltaT, mechanics_->nextDeltaT());
            deltaT = min(deltaT, heatSrc_->nextDeltaT());
            deltaT = min(deltaT, burnup_->nextDeltaT());
            deltaT = min(deltaT, fgr_->nextDeltaT());

            adjustableTime.setDeltaT(deltaT);
        }     
    }

    Info<< "\n end \n";

    return 0;
}


// ************************************************************************* //

