# Temperature Boundary Conditions {#temperatureBCs}

This page summarizes the thermal boundary conditions currently available in OFFBEAT. Detailed information on the usage of the boundary conditions can be found in the description of their header file ".H". All the available options are quickly visible using the "banana method", i.e. using a dummy keywords (e.g "banana") for the boundary condition type. Running the case OpenFOAM will detect the mistake and propose a set of possible valid alternatives to the dummy keyword.

### Fixed temperature boundary conditions
BCs developed to deal with fix or time-dependent temperature profiles. These boundaries are typically employed for the outer cladding surface (when the temperature profiles are known) and take into account the added thermal resistance of the corrosion layer if the corrosion model is activated.

| Boundary Condition Name      | Brief Description                                                                                     | Class                                         |
|------------------------------|-------------------------------------------------------------------------------------------------------|-----------------------------------------------|
| `fixedTemperature`           | Imposes a fixed temperature on the patch. NOTE: See ".H" file for info on oxideLayer.                 | fixedTemperatureFvPatchScalarField.H          |
| `timeDependentTemperature`   | Imposes a fixed temperature history on the patch. See ".H" file for info on oxideLayer.               | timeDependentTemperatureFvPatchScalarField.H  |
| `axialProfileT`              | Imposes a fixed temperature axial profile on the patch. See ".H" file for info on oxideLayer.         | axialProfileFvPatchScalarField.H              |
| `timeDependentAxialProfileT` | Imposes a fixed axial temperature profile history on the patch. See ".H" file for info on oxideLayer. | timeDependentAxialProfileFvPatchScalarField.H |

### Heat exchange boundary conditions
BCs developed to simulate the (simplified) heat exchange between cladding and water: a fix or time-dependent heat exchange coefficient is provided to the code together with a fix or time-dependent coolant temperature. Axial profiles can be added as well.

| Boundary Condition Name        | Brief Description                                                                                                             | Class                                            |
|--------------------------------|-------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------|
| `convectiveHTC`                | Heat sink boundary defined by a convective heat transfer coefficient and a sink temperature.                                  | convectiveHTCFvPatchScalarField.H                |
| `timeDependentAxialProfileHTC` | Heat sink boundary defined by an axial profile of convective heat transfer coefficients and a sink temperature axial profile. | timeDependentAxialProfileHTCfvPatchScalarField.H |
| `radiativeConvectiveSink`      | Same as `convectiveHTC`, but with the addition of radiative heat transfer.                                                    | radiativeConvectiveSinkFvPatchScalarField.H      |

### Coupled Boundary conditions
BCs developed for treating (primarily) the heat exchange between fuel and cladding across the gap. Some of them can be be used also to treat the heat conduction between stacked fuel pellets.

| Boundary Condition Name  | Brief Description                                                                                            | Class                                  |
|--------------------------|--------------------------------------------------------------------------------------------------------------|----------------------------------------|
| `temperatureCoupled`     | BC that forces continuity (for the temperature field) between two bodies with different material properties. | temperatureCoupledFvPatchScalarField.H |
| `resistiveGap`           | BC that simulate the presence of a fixed gap (or contact) resistance between two bodies.                     | resistiveGapFvPatchScalarField.H       |
| `fuelRodGap`             | BC that computes the gap conductance as sum of contact, radiative and gas conductive contributions.          | fuelRodGapFvPatchScalarField.H         |


***

Return to [Fields and Boundary Conditions ](@ref fieldsAndBCs)
