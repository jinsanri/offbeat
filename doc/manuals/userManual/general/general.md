# General Instructions {#general}

OFFBEAT operation is similar to typical solvers shipped with OpenFOAM (e.g. icoFoam, pisoFoam, etc.) in that the user provides a mesh, control dictionary `controlDict`, solution parameters `fvSolution` and schemes dictionary `fvSchemes` along with an OFFBEAT-specific solver dictionary `solverDict`, and with intial and boundary conditions for the main fields in the initial time step folder (e.g. the folder `0/`).

* [Folder Structure](@ref folderStructure)
* [Computational Domain, Mesh and Boundary Definitions](@ref computationalDomain)
* [Workflow](@ref workflow)

***

Return to [User Manual](@ref userManual)
