# Burnup Model {#burnup}
The burnup model type is selected with the `burnup` keyword in the `solverDict` dictionary. The burnup model handles the burnup field `BU` and its increase during irradiation. 

**NOTE:** Burnup's units in OFFBEAT are (MWd/MT) and not (MWd/MT_U) where MT_U stands for 'mega-tons of Uranium'.

Currently OFFBEAT supports the following burnup models:
- By selecting the mother class burnup.H with the keyword `fromLatestTime`, the burnup field remains as defined in the initial time folder (by default equal to 0 MWd/tUO2 if the `BU` file is missing).
- burnupFromPower.H , the burnup is calculated integrating the power over time.
- burnupLassmann.H , the burnup is calculated integrating the power over time and the concentration of a few fissile isotopes is tracked. The power form factor is provided by the [neutronics solver](@ref neutronics) (which computes the shape of the thermal neutron flux) and from a radial profile to account for epithermal captures.

***

Return to [Setting the `solverDict`](@ref solverDict)
