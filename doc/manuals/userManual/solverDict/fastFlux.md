# Fast Flux and Fast Fluence Models {#fastFlux}
The fast flux model type is selected with the `fastFlux` keyword in the *solverDict* dictionary. The fast flux model also calculates the fast fluence, integrating the fast flux over time. The units of the fast flux are (n/cm2/s). The units of the fast fluence are (n/cm2).

Currently OFFBEAT supports the following fast flux models:
- By selecting the mother class fastFlux.H with the keyword `fromLatestTime`, the fast flux field remains as defined in the initial time folder (by default equal to 0 n/cm2/s if the `fastFlux` file is missing). Note that when `fromLatestTime` is selected, the fast fluence keeps being updated at each time step as:
\f[
		fastFluence(t) = fastFluence(t-\Delta t) + fastFlux\cdot \Delta t
\f]
- timeDependentAxialProfile.H , time dependent average fast flux. It offers the possibility of having an axial profile. When selecting the `timeDependentAxialProfile` fast flux model, the fast flux is provided as a combination of rod-average value and axial profiles:
\f[
		fastFlux(t, z) = fastFlux(t)\cdot f(z,t)
\f]

where:
    - \f$ z \f$ is the relative axial position (0...1)
    - \f$ t \f$ is the current time
    
The axial profiles is expected to be normalised to 1.


***

Return to [Setting the `solverDict`](@ref solverDict)
