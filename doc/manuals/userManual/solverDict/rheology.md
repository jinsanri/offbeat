# Rheology Model {#rheology}
The `rheology` keyword in the *solverDict* dictionary selects the overall treatement of rheological properties in OFFBEAT. Currently the code supports only the option:
- `byMaterial` (see rheologyByMaterial.H), i.e. the specific rheological model (e.g. elasticity) is selected at the material level (typically by choosing the appropriate value for the keyword `rheologyModel` in the `materials` subdictionary).

Additional entries might be required in the `rheologyOptions` subdictionary (they are listed below in the example of use).

## Constitutive laws

Currently, the following consitutive laws are available in OFFBEAT for modeling linear-elastic materials (typically, but not necessarily, in a small-strain framework):
- elasticity.H , linear elasticity following Hooke's law is assumed. Note that even if the constitutive law is linear-elastic, the total strain field might include additional components, such as thermal strain or swelling.
- misesPlasticity.H , an additional (instantaneous) plastic strain component is calculated as a function of the stress state and of the cumulated plastic strain itself. Hooke's law is assumed to calculate the stress as a function of the elastic strain.
- misesPlasticCreep.H, an additional time-dependent plastic strain component is calculated according to the specific creep model selected. Von Mises theory is used for the instantaneous portion of the plastic strain, while Hooke's law is assumed for calculating the stress as a function of the (remaining) elastic strain component.

Currently, the following consitutive laws are available in OFFBEAT for modeling materials for which the linear-elastic approximation is not accurate (usually in a large-strain framework):
- hyperElasticity.H , the stress follows a StVenant-Kirchhoff hyperElasticity constitutive law.
- hyperElasticMisesPlasticCreep.H, a time-dependent plastic strain component is calculated according to the specific creep model selected. The total strain is considered to be additive. The creep and additional strain component are removed from the total strain tensor, and the stress is calculated following a StVenant-Kirchhoff hyperElasticity constitutive law as a function of the (remaining) elastic strains.
- neoHookeanElasticity.H, the stress follows a neoHookean elasticity constitutive law.
- neoHookeanMisesPlasticity.H, Mises plasticity is used for the plastic component, whil the stress follows a neoHookean elasticity constitutive law.

Note that when modeling a fuel rod in large strain, it is typically a good approximation to select `misesPlasticCreep` law. Neo hookean laws are strongly not suggested (they do not represent the behavior of fuel and cladding) and they are present in OFFBEAT mainly for benchmark and verification purposes.

### Plasticity

When selecting `misesPlasticity`, `hyperElasticMisesPlasticCreep` or `neoHookeanMisesPlasticity`, it is necessary to specify how the yield stress varies as a function of the cumulated plastic strain. This is done by providing a `plasticStrainVsYieldStress` table. The table can be introduced as an OpenFOAM format table or as a .csv file. For complex yield stress vs plastic strain dependency, the user might prefer to have these data on a separate file and incorporate it in the `solverDict` with `#include`.

### Creep

The class handling the treatement of the creep strains are derived from `misesPlasticity`, therefore the options described above also apply.
Additional entries might be required in the `rheologyModelOptions` subdictionary (they are listed below in the example of use).

Currently, the creep models implemented in OFFBEAT are:
- LimbackCreepModel.H , thermal and irradiation creep following the correlations of the Limbàck model (for Zircaloy).
- LimbackLOCA.H , thermal and irradiation creep following the correlations of the Limbàck model including an extension for high-temperature/large-strain regime (for Zircaloy). In the future the two Limback models will be merged.
- MatproCreepModel.H , thermal and irradiation creep for UO2 following the correlations found in MATPRO.

***

Return to [Setting the `solverDict`](@ref solverDict)
