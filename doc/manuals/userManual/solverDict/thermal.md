# Thermal Solution {#thermal}

The energy conservation equation is the governing law for thermal solution in OFFBEAT, determining the distribution of temperatures in the nuclear fuel rod. In the current version of the code, a single solid conduction solver ( solidConductionSolver.H ) is available.
OFFBEAT adopts a Lagrangian approach, thus the convection terms due to the flow of mass across the surfaces are not considered. In this framework, the conservation of energy for a solid body with arbitrary shape and volume V, bounded by a surface S with outward normal n, can be expressed in the form of the heat diffusion equation:
\f[
	\int_V{\frac{\partial \rho c_p T}{\partial t}dV} = \oint_S{n\cdot q'' dS} + \int_V{q'''dV}
\f]

where:
* \f$ \rho \f$ is the density
* \f$ c_p \f$ is the specific heat capacity
* \f$ q'' \f$ is the heat flux flowing through the surface S
* \f$ q''' \f$ is the volumetric heat source.

The heat flux is defined as:
\f[
	q'' = \kappa \nabla T
\f]

where \f$ \kappa \f$ is the thermal conductivity. The heat diffusion equation above states that the rate of change of thermal energy stored in the volume V is equal to net rate of energy entering the body through the surface S plus the rate of energy generated.

The thermal solution can be deactivated by selecting the parent class 

 It can be selected with the "fromLatestTime" typeName in solverDict. 
    In this case, the temperature field "T" is read from latest time folder 
    and no equation is solved for. If the file is not present, the temperature
    is set to 0 by default with zero-gradient BCs.

***

Return to [Setting the `solverDict`](@ref solverDict)
