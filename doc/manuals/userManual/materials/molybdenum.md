# Molybdenum material model {#molybdenum}

Material model for Molybdenum material (see molybdenum.H). The model uses default selections for thermomechanical properties and behavioral models, but different choices can be selected by the user. 

The default selections of the material model are listed in the table below.

Please refer to the header of each listed file for more information on the specific correlation/model.

### Thermomechanical properties

| Thermomechanical property | Default model                    |
|---------------------------|----------------------------------|
| Thermal Conductivity      | conductivityMolybdenum.H         |
| Density                   | constantDensityMolybdenum.H      |
| Emisssivity               | constantEmissivityMolybdenum.H   |
| Heat Capacity             | heatCapacityMolybdenum.H         |
| Poisson's Ratio           | constantPoissonRatioMolybdenum.H |
| Thermal Expansion         | thermalExpansionMolybdenum.H     |
| Young's Modulus           | YoungModulusMolybdenum.H         |

### Behavioral models

| Behavioral phenomenon     | Default model                 |
|---------------------------|-------------------------------|
| Swelling                  | swellingFrCrAl.H              |
