# Makefile
PROJECT = offbeat


all :
	@echo Making all for project $(PROJECT)
	@echo Build type set to $(WM_COMPILE_OPTION)
	wmakeLnInclude SCIANTIX
	wmake -j SCIANTIX
	wmakeLnInclude offbeatLib
	wmake -j offbeatLib
	wmake -j offbeat
# 	make -C doc/Doxygen all

clean :
	@echo Cleaning project $(PROJECT) 
	wclean offbeat	
	wclean offbeatLib
	wclean SCIANTIX
# 	make -C doc/Doxygen clean
